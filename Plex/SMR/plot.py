###!/usr/bin/env python
import sys, os, math, glob
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker
from numpy import array
plt.close()
idx = -1
tflux = [] 
sizes = []
for filename in glob.glob('out-*'):
    idx = idx + 1
    words = filename.split('-')
    sz = int(words[2])
    sizes.append(sz)
    tflux.append([])
    for text in open(filename,"r"): 
        words = text.split()
        n = len(words)
        if n > 1 and words[1] == 'Total':
            tm = float(words[11])
            tflux[idx].append(tm)
print sizes
plt.figure(1)
ax = plt.subplot(211)
plt.ylabel('Thermal Flux')
#plt.xlabel('Time')
plt.title('Thermal Flux')
n = len(sizes)
diffs = []
for i in xrange(n):
    xx = array(range(len(tflux[i])))*1.e-3
    plt.plot(xx,tflux[i], label= repr(sizes[i]))
    #plt.plot(tflux[i], label= repr(sizes[i]) )
    if i > 0:
        diff = [math.fabs(x - y) for x, y in zip(tflux[i], tflux[i-1])]
        diffs.append(diff)
ax.legend(ncol=2,loc="lower left", bbox_to_anchor=[0,0],shadow=False, fancybox=True,title="Number of cells")
ax.axis([0, 0.1, 0, .05])
ax = plt.subplot(212)
plt.ylabel('Thermal Flux differences')
plt.xlabel('Time')
#plt.title('Mesh sequencing differences')
for i in xrange(n-1):
    xx = array(range(len(diffs[i])))*1.e-3
    plt.semilogy(xx,diffs[i], label = repr(sizes[i+1]) + ' - ' + repr(sizes[i]) + ' cell fluxes' )
ax.legend(loc="lower left", bbox_to_anchor=[0, 0],shadow=False, fancybox=True)
ax.grid(color='c', linestyle='--', linewidth=1)
ax.legend( loc="lower right", shadow=False, fancybox=True)
ax.axis([0, 0.1, 1.e-10, 1.e-2])
# plt.show()
plt.savefig('thermal_fluxes.png')
# convergence plot
exact = [17.*x1/16. - x0/16. for x0, x1 in zip(tflux[n-2], tflux[n-1])]
tar = len(exact) - 1
print 'n = ', n, 'target = ', tar
errors = []
quad = []
for i in xrange(n):
    err = math.fabs((exact[tar] - tflux[i][tar])/exact[tar])
    errors.append(err)
    N = 8*pow(2,i)
    quad.append(90*math.pow(1./N,4.))
plt.figure(2)
ax = plt.subplot(111)
x = sizes
plt.loglog(x, errors, 'bo--', label= '|u~ - u|/|u|')
plt.loglog(x, quad, 'r--', label= 'Quartic')
ax.legend(loc="upper right", shadow=False, fancybox=True)
ax.axis([sizes[0]-10, sizes[n-1]+3000, 1.e-7, 1.e-0])
plt.ylabel('|u~ - u|/|u| of thermal flux u')
plt.xlabel('number of cells')
plt.title('Convergence rate of thermal flux, T = ' + repr(tar*1.e-3) )
plt.savefig('thermal_flux_convergance.png')
#tick_spacing = 1
#ax.xaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
labels = repr(sizes)
# You can specify a rotation for the tick labels in degrees or with keywords.
plt.xticks(x, x, rotation='horizontal')
plt.show()
