#if !defined(__LANDAU_H)
#define __LANDAU_H

#include <petscts.h>
#include <petscdmplex.h>
#include <petscds.h>
#include <petsc/private/petscfeimpl.h>    /*I   "petscfe.h"   I*/
#include <petsc/private/snesimpl.h>     /*I "petscsnes.h"   I*/
#include <assert.h>
#define LAND_NUM_SPECIES_MAX 3
typedef struct {
  PetscReal    *xr;
  PetscReal    *xz;
  PetscReal    *f[LAND_NUM_SPECIES_MAX];
  PetscReal    *df[2][LAND_NUM_SPECIES_MAX];
  PetscInt     nip; /* number of integration points */
  PetscInt     ns; /* number of species or fields */
} LandPointData;
/* the context */
typedef struct {
  PetscBool     interpolate;                  /* Generate intermediate mesh elements */
  PetscBool     simplex;
  PetscBool     matrix_free_residual;
  PetscInt      verbose;
  DM            dm;
  DM            serial_dm;                   /* Serial DM for evaluation of dense kernels */
  PetscFE       fe[LAND_NUM_SPECIES_MAX];
  PetscLogEvent events[20];
  /* timestepping  */
  PetscInt      current_step;
  PetscReal     current_time;
  /* geometry  */
  PetscReal     inner_mult;
  PetscReal     radius;
  PetscBool     use_cartesian;
  PetscBool     use_amr;
  /* physics */
  PetscReal     m_0;      /* reference mass */
  /* PetscReal     mi_me; */    /* mass ratio (1835.5) */
  PetscReal     epsilon0; /* permittivity of free space (MKS) */
  PetscReal     masses[LAND_NUM_SPECIES_MAX]; /* mass of each species  */
  PetscReal     charges[LAND_NUM_SPECIES_MAX]; /* charge of each species */
  PetscReal     thermal_temps[LAND_NUM_SPECIES_MAX];
  PetscReal     Ez;
  PetscInt      num_species;
  /* amr */
  PetscReal     amr_factors[LAND_NUM_SPECIES_MAX];
  PetscInt      num_min_cells;
  /* cache */
  Mat           J;
  Mat           M;
  PetscReal     normJ;
  /* discretization */
  PetscErrorCode (*errorIndicator)(PetscInt, PetscReal, PetscInt, const PetscInt[], const PetscScalar[], const PetscScalar[], PetscReal *, void *);
  /* MPI */
  MPI_Comm      comm;
  PetscMPIInt   sub_rank;
  PetscInt      sub_index;
  /* names */
  char          filename[PETSC_MAX_PATH_LEN]; /* Mesh filename */
  char          plot_file_prefix[16]; /* plot file prefix */
} LandCtx;
PETSC_EXTERN PetscErrorCode LandPointDataView(MPI_Comm,LandPointData*);
PETSC_EXTERN PetscErrorCode LandPointDataCreate(LandPointData*,PetscInt,PetscInt,PetscInt);
PETSC_EXTERN PetscErrorCode LandPointDataDestroy(LandPointData*);

PETSC_EXTERN PetscErrorCode LandIFunction(TS ts,PetscReal t,Vec u,Vec u_t,Vec F,void *ctx);
PETSC_EXTERN PetscErrorCode LandIJacobian(TS ts,PetscReal t,Vec U,Vec U_t,PetscReal a,Mat Amat,Mat Pmat,void *ctx);


#if defined ( __AVX512F__ )
#if defined(PETSC_USE_REAL_SINGLE)
#define LAND_VL 16
#define _MAVX_  __m512
#define M_SET1   _mm512_set1_ps
#define M_STORE  _mm512_store_ps
#define M_ZERO   _mm512_setzero_ps
#define M_LOAD   _mm512_load_ps
#define M_MULT   _mm512_mul_ps
#define M_FMA    _mm512_fmadd_ps
#define M_ADD    _mm512_add_ps
#define M_SUB    _mm512_sub_ps
#define M_SQRT   _mm512_sqrt_ps
#define M_ISQRT  _mm512_invsqrt_ps
#define M_ANDNOT _mm512_andnot_ps
#define M_AND    _mm512_and_ps
#define M_OR     _mm512_or_ps
#define M_DIV    _mm512_div_ps
#define M_LOG    _mm512_log_ps
#define M_RECIP(xx) _mm512_div_ps(s_one,xx)
#define M_CMPNE(a,b) _mm512_maskz_cvtepi64_ps(_mm512_cmp_ps_mask(a,b,_MM_CMPINT_NE),_mm512_maskz_set1_epi16(0xFFFFFFFF,1))
#else
#define LAND_VL 8
#define _MAVX_   __m512d
#define M_SET1   _mm512_set1_pd
#define M_STORE  _mm512_store_pd
#define M_ZERO   _mm512_setzero_pd
#define M_LOAD   _mm512_load_pd
#define M_MULT   _mm512_mul_pd
#define M_FMA    _mm512_fmadd_pd
#define M_ADD    _mm512_add_pd
#define M_SUB    _mm512_sub_pd
#define M_SQRT   _mm512_sqrt_pd
#define M_ISQRT  _mm512_invsqrt_pd
#define M_ANDNOT _mm512_andnot_pd
#define M_AND    _mm512_and_pd
#define M_OR     _mm512_or_pd
#define M_DIV    _mm512_div_pd
#define M_LOG    _mm512_log_pd
#define M_RECIP(xx) _mm512_div_pd(s_one,xx)
#define M_CMPNE(a,b) _mm512_cmp_pd_mask(a, b, _CMP_NEQ_UQ)
#endif
#elif defined ( __AVX__ )
#if defined(PETSC_USE_REAL_SINGLE)
#define LAND_VL 4
#define _MAVX_  __m128
#define M_SET1   _mm_set1_ps
#define M_ZERO   _mm_setzero_ps
#define M_LOAD   _mm_load_ps
#define M_MULT   _mm_mul_ps
#define M_FMA(a,b,c) _mm_add_ps(_mm_mul_ps(a,b),c)
#define M_STORE  _mm_store_ps
#define M_ADD    _mm_add_ps
#define M_SUB    _mm_sub_ps
#define M_SQRT   _mm_sqrt_ps
#define M_ISQRT(xx) (_mm_div_ps(s_one,_mm_sqrt_ps(xx)))
#define M_ANDNOT _mm_andnot_ps
#define M_AND    _mm_and_ps
#define M_OR     _mm_or_ps
#define M_DIV    _mm_div_ps
#define M_RECIP(xx) _mm_div_ps(s_one,xx)
#define M_CMPGT(a,b) _mm_cmpgt_ps(a,b)
#define M_CMPLT(a,b) _mm_cmplt_ps(a,b)
#define M_CMPNE(a,b) _mm_cmpneq_ps(a,b)
#else
#define LAND_VL 2
#define _MAVX_  __m128d
#define M_SET1   _mm_set1_pd
#define M_ZERO   _mm_setzero_pd
#define M_LOAD   _mm_load_pd
#define M_MULT   _mm_mul_pd
#define M_FMA(a,b,c) _mm_add_pd(_mm_mul_pd(a,b),c)
#define M_STORE  _mm_store_pd
#define M_ADD    _mm_add_pd
#define M_SUB    _mm_sub_pd
#define M_SQRT   _mm_sqrt_pd
#define M_ISQRT(xx) (_mm_div_pd(s_one,_mm_sqrt_pd(xx)))
#define M_ANDNOT _mm_andnot_pd
#define M_AND    _mm_and_pd
#define M_OR     _mm_or_pd
#define M_DIV    _mm_div_pd
#define M_RECIP(xx) _mm_div_pd(s_one,xx)
#define M_CMPGT(a,b) _mm_cmpgt_pd(a,b)
#define M_CMPLT(a,b) _mm_cmplt_pd(a,b)
#define M_CMPNE(a,b) _mm_cmpneq_pd(a,b)
#endif

PETSC_STATIC_INLINE _MAVX_ M_LOG( _MAVX_ x ) {
#if defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 1300)
  __declspec(align(PETSC_MEMALIGN))
#endif
  PetscReal ax[LAND_VL],lax[LAND_VL];
  int ii;
  M_STORE(ax, x);
  for (ii = 0; ii < LAND_VL; ii++) lax[ii] = log(ax[ii]);
  x = M_LOAD(lax);
  return x;
}
#else 
#define LAND_VL 1
#error
#endif

#endif
