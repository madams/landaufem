static char help[] = "Fokker-Plank-Landau collision operator\n\n";

#include "landau.h"
#include <petscdmforest.h>
#include <petsc/private/dtimpl.h>     /*I "petscdt.h"   I*/
/* #define LAND_ADD_BCS */
PetscInt s_species_idx = 0;

// Define a Maxwellian function for testing out the operator.
//
// Using cartesian velocity space coordinates, the particle
// density, [1/m^3], is defined according to 
//
// $$ n=\int_{R^3} dv^3 \left(\frac{m}{2\pi T}\right)^{3/2}\exp [- mv^2/(2T)] $$
//
// Using some constant, c, we normalize the velocity vector into a
// dimensionless variable according to v=c*x. Thus the density, $n$, becomes
//
// $$ n=\int_{R^3} dx^3 \left(\frac{mc^2}{2\pi T}\right)^{3/2}\exp [- mc^2/(2T)*x^2] $$
//
// Defining $\theta=2T/mc^2$, we thus find that the probability density 
// for finding the particle within the interval in a box dx^3 around x is 
//
// f(x;\theta)=\left(\frac{1}{\pi\theta}\right)^{3/2} \exp [ -x^2/\theta ]
//
#undef __FUNCT__
#define __FUNCT__ "bimaxwellian"
PetscErrorCode bimaxwellian(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  PetscInt i;
  PetscReal *p = (PetscReal*)ctx, theta = p[0], fact = p[1], d = p[2];
  const double shift[3]={0,d,0}; /* for defining a shifting Maxwellian (opposite sign from bimaxwellian) */
  double tmp1 = 0.;
  double tmp2 = 0.;
  PetscFunctionBeginUser;
  /* compute the exponents */
  for (i = 0; i < dim; ++i) {
    tmp2 += (x[i]+shift[i])*(x[i]+shift[i]);
  }
  /* evaluate the double-peaked Maxwellian */
  if (d>0) {
    for (i = 0; i < dim; ++i) {
      tmp1 += x[i]*x[i];
    }
    u[0] = fact*0.5*pow(M_PI*theta,-1.5)*(exp(-tmp1/theta) + exp(-tmp2/theta));
  } else {
    u[0] = fact*0.5*pow(M_PI*theta,-1.5)*(exp(-tmp2/theta));
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "g0_r"
/* < v, u > */
static void g0_r(PetscInt dim, PetscInt Nf, PetscInt NfAux,
		 const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
		 const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
		 PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g0[])
              /* PetscReal t, PetscReal u_tShift, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar g0[] */
{
  g0[0] = x[0];
}

#undef __FUNCT__
#define __FUNCT__ "f0_rden"
/* < v, ru > */
static void f0_rden(PetscInt dim, PetscInt Nf, PetscInt NfAux,
		    const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
		    const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
		    PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
                 /* PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar obj[]) */
{
  f0[0] = x[0]*u[s_species_idx];
}

#undef __FUNCT__
#define __FUNCT__ "f0_rmom"
/* < v, ru > */
static void f0_rmom(PetscInt dim, PetscInt Nf, PetscInt NfAux,
		    const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
		    const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
		    PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  *f0 = x[0]*x[1]*u[s_species_idx];
}

#undef __FUNCT__
#define __FUNCT__ "f0_re"
static void f0_re(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
                  const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
                  PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  *f0 = x[0]*(x[0]*x[0] + x[1]*x[1])*u[s_species_idx];
}

#undef __FUNCT__
#define __FUNCT__ "f0_rtherm"
static void f0_rtherm(PetscInt dim, PetscInt Nf, PetscInt NfAux,
		      const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[], const PetscScalar u_x[],
		      const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
		      PetscReal t, const PetscReal x[], PetscInt numConstants, const PetscScalar constants[], PetscScalar f0[])
{
  *f0 = x[0]*x[1]*(x[0]*x[0] + x[1]*x[1])*u[s_species_idx]; /* is this meaningful? */
}

#if defined(LAND_ADD_BCS)
PETSC_STATIC_INLINE PetscErrorCode zero(PetscInt dim, PetscReal time, const PetscReal x[], PetscInt Nf, PetscScalar *u, void *ctx)
{
  PetscInt d;
  for (d = 0; d < Nf; ++d) u[d] = 0.;
  return 0;
}
#endif
#if 0
static PetscReal s_points2[][2] = {
  {0.33333333333333, 0.33333333333333},
  {0.47014206410511, 0.47014206410511},
  {0.47014206410511, 0.05971587178977},
  {0.05971587178977, 0.47014206410511},
  {0.10128650732346, 0.10128650732346},
  {0.10128650732346, 0.79742698535309},
  {0.79742698535309, 0.10128650732346}
};
static PetscReal s_weights2[] = {0.22500000000000,
				 0.13239415278851,
				 0.13239415278851,
				 0.13239415278851,
				 0.12593918054483,
				 0.12593918054483,
				 0.12593918054483
};
#else
static PetscReal s_points2[][2] = {
  {0.44594849091597, 0.44594849091597},
  {0.44594849091597, 0.10810301816807},
  {0.10810301816807, 0.44594849091597},
  {0.09157621350977, 0.09157621350977},
  {0.09157621350977, 0.81684757298046 },
  {0.81684757298046, 0.09157621350977 }
};
static PetscReal s_weights2[] = {0.22338158967801,
				 0.22338158967801,
				 0.22338158967801,
				 0.10995174365532,
				 0.10995174365532,
				 0.10995174365532
};
#endif
static PetscReal s_points1[][2] = {
  {0.333333333333333, 0.333333333333333} };
static PetscReal s_weights1[] = {1.};

#undef __FUNCT__
#define __FUNCT__ "LandFEChangeQuadrature"
static PetscErrorCode LandFEChangeQuadrature(PetscFE fe)
{
  PetscErrorCode  ierr;
  PetscQuadrature quad;
  PetscSpace      P;
  PetscBool       tensor;
  PetscInt        dim,i,j,k,numP;
  PetscReal       (*qp)[2] = 0,*qw = 0;
  PetscFunctionBeginUser;
  ierr = PetscFEGetQuadrature(fe, &quad);CHKERRQ(ierr);
  ierr = PetscFEGetBasisSpace(fe, &P);CHKERRQ(ierr);
  ierr = PetscSpacePolynomialGetTensor(P, &tensor);CHKERRQ(ierr);
  ierr = PetscFEGetSpatialDimension(fe, &dim);CHKERRQ(ierr);
  if (!tensor) {
    if (quad->numPoints==9) {
      numP = sizeof(s_points2)/(dim*sizeof(PetscReal));
      qp = s_points2;
      qw = s_weights2;
    } else if (quad->numPoints==4) {
      numP = sizeof(s_points1)/(dim*sizeof(PetscReal));
      qp = s_points1;
      qw = s_weights1;
    }
  }
  if (qp) {
    PetscReal       *qp2 = (PetscReal*)quad->points;
    PetscReal       *qw2 = (PetscReal*)quad->weights;
    for (i=0,k=0;i<numP;i++) {
      for (j=0;j<dim;j++,k++) {
        qp2[k] = 2*qp[i][j] - 1;
      }
      qw2[i] = 2*qw[i];
    }
    for (/* void */;i<quad->numPoints;i++) {
      qw2[i] = 0;
    }
    quad->numPoints = numP;
    ierr = PetscPrintf(PETSC_COMM_WORLD, " *** %s Hardwire to %D point quadrature\n",__FUNCT__,numP);CHKERRQ(ierr);
  } else {
    /*     ierr = PetscPrintf(PETSC_COMM_WORLD, " *** %s No hack. %s Nq=%D ***\n",__FUNCT__,tensor ? "Tensor" : "Simplex",quad->numPoints);CHKERRQ(ierr); */
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LandCreateMassMatrix"
static PetscErrorCode LandCreateMassMatrix(LandCtx *ctx, Vec X, Mat *Amat)
{
  PetscDS        prob;
  PetscFE        fe;
  PetscErrorCode ierr;
  PetscInt       dim,N1=1,N2,ii;
  const char     prefix[] = "mass_";
  PetscFunctionBeginUser;
  ierr = DMGetDimension(ctx->dm, &dim);CHKERRQ(ierr);
  ierr = DMGetDS(ctx->dm, &prob);CHKERRQ(ierr);
  for (ii=0;ii<ctx->num_species;ii++) {
    ierr = PetscFECreateDefault(ctx->dm, dim, 1, ctx->simplex, prefix, PETSC_DECIDE, &fe);CHKERRQ(ierr);
    ierr = LandFEChangeQuadrature(fe);CHKERRQ(ierr);
    ierr = PetscDSSetDiscretization(prob, ii, (PetscObject) fe);CHKERRQ(ierr); /* this pollutes ctx but DMClone messes up matrix structure with S>3 */
    ierr = PetscDSSetJacobian(prob, ii, ii, g0_r, NULL, NULL, NULL);CHKERRQ(ierr);
#if defined(LAND_ADD_BCS)
    ierr = PetscDSAddBoundary(prob, DM_BC_ESSENTIAL, "wall", "marker", ii, 0, NULL, (void (*)()) zero, 1, &N1, ctx);CHKERRQ(ierr);
#endif
    ierr = PetscFEDestroy(&fe);CHKERRQ(ierr);
  }
  ierr = DMCreateMatrix(ctx->dm, Amat);CHKERRQ(ierr);
  {
    Vec locX;
    DM  plex;
    ierr = DMConvert(ctx->dm, DMPLEX, &plex);CHKERRQ(ierr);
    ierr = DMCreateLocalVector(ctx->dm, &locX);CHKERRQ(ierr);
    ierr = DMGlobalToLocalBegin(ctx->dm, X, INSERT_VALUES, locX);CHKERRQ(ierr);
    ierr = DMGlobalToLocalEnd  (ctx->dm, X, INSERT_VALUES, locX);CHKERRQ(ierr);
    ierr = DMPlexSNESComputeJacobianFEM(ctx->dm, locX, *Amat, *Amat, ctx);CHKERRQ(ierr);
    ierr = VecDestroy(&locX);CHKERRQ(ierr);
    ierr = DMDestroy(&plex);CHKERRQ(ierr);
  }
  ierr = MatGetSize(ctx->J, &N1, NULL);CHKERRQ(ierr);
  ierr = MatGetSize(*Amat, &N2, NULL);CHKERRQ(ierr);
  if (N1 != N2) SETERRQ2(PetscObjectComm((PetscObject) ctx->dm), PETSC_ERR_PLIB, "Incorrect matrix sizes: |Jacobian| = %D, |Mass|=%D",N1,N2);
  ierr = MatViewFromOptions(*Amat,NULL,"-mass_mat_view");CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LandDMVecView"
static PetscErrorCode LandDMVecView(Vec X, PetscInt stepi, const char prefix[])
{
  PetscErrorCode    ierr;
  PetscViewer       viewer = NULL;
  char              buf[256];
  PetscBool         isHDF5,isVTK;
  LandCtx           *ctx=0;
  PetscInt          nloc,ii,jj;
  PetscScalar       *a;
  DM                dm;
  PetscFunctionBegin;
  ierr = VecGetDM(X, &dm);CHKERRQ(ierr);
  ierr = PetscViewerCreate(PetscObjectComm((PetscObject)dm),&viewer);CHKERRQ(ierr);
  ierr = PetscViewerSetType(viewer,PETSCVIEWERVTK);CHKERRQ(ierr);
  ierr = PetscViewerSetFromOptions(viewer);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERHDF5,&isHDF5);CHKERRQ(ierr);
  ierr = PetscObjectTypeCompare((PetscObject)viewer,PETSCVIEWERVTK,&isVTK);CHKERRQ(ierr);
  if (isHDF5) {
    ierr = PetscSNPrintf(buf, 256, "%s-%d.h5", prefix, stepi);CHKERRQ(ierr);
  } else if (isVTK) {
    ierr = PetscSNPrintf(buf, 256, "%s-%d.vtu", prefix, stepi);CHKERRQ(ierr);
    ierr = PetscViewerPushFormat(viewer,PETSC_VIEWER_VTK_VTU);CHKERRQ(ierr);
  }
  ierr = PetscViewerFileSetMode(viewer,FILE_MODE_WRITE);CHKERRQ(ierr);
  ierr = PetscViewerFileSetName(viewer,buf);CHKERRQ(ierr);
  if (isHDF5) {
    ierr = DMView(dm,viewer);CHKERRQ(ierr);
    ierr = PetscViewerFileSetMode(viewer,FILE_MODE_UPDATE);CHKERRQ(ierr);
  }
  ierr = DMGetApplicationContext(dm, &ctx);CHKERRQ(ierr); assert(ctx);
  /* PetscInt bs; ierr = VecGetBlockSize(X,&bs);CHKERRQ(ierr); assert(bs==ctx->num_species); */
  ierr = VecGetLocalSize(X,&nloc);CHKERRQ(ierr);
  /* scale by charge */
  ierr = VecGetArray(X,&a);CHKERRQ(ierr);
  for (ii=0;ii<nloc;ii+=ctx->num_species) for (jj=0;jj<ctx->num_species;jj++) a[ii+jj] *= -ctx->charges[jj]/ctx->charges[0];
  ierr = VecRestoreArray(X,&a);CHKERRQ(ierr);
  /* view */
  ierr = VecView(X,viewer);CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&viewer);CHKERRQ(ierr);
  /* scale back */
  ierr = VecGetArray(X,&a);CHKERRQ(ierr);
  for (ii=0;ii<nloc;ii+=ctx->num_species) for (jj=0;jj<ctx->num_species;jj++) a[ii+jj] /= -ctx->charges[jj]/ctx->charges[0];
  ierr = VecRestoreArray(X,&a);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LandView"
static PetscErrorCode LandView(DM dm_dummy, Vec X, PetscInt stepi, LandCtx *ctx)
{
  PetscFunctionBegin;
  if (ctx->verbose > 1) {
    PetscErrorCode ierr;
    PetscDS        prob;
    DM             plex,serial_plex;
    PetscScalar    momentumtot=0, energytot=0, densitytot=0, thermtot=0;
    PetscInt       cStart, cEnd;
    /* print momentum and energy */
    ierr = DMConvert(ctx->dm, DMPLEX, &plex);CHKERRQ(ierr);
    ierr = DMConvert(ctx->serial_dm, DMPLEX, &serial_plex);CHKERRQ(ierr);
    ierr = DMGetDS(plex, &prob);CHKERRQ(ierr);
    for (s_species_idx=0;s_species_idx<ctx->num_species;s_species_idx++) {
      PetscScalar momentum, energy, density, therm, tt[LAND_NUM_SPECIES_MAX];
      ierr = PetscDSSetObjective(prob, 0, &f0_rden);CHKERRQ(ierr);
      ierr = DMPlexComputeIntegralFEM(plex,X,tt,ctx);CHKERRQ(ierr);
      density = tt[0]*ctx->charges[s_species_idx]/ctx->charges[0];
      ierr = PetscDSSetObjective(prob, 0, &f0_rmom);CHKERRQ(ierr);
      ierr = DMPlexComputeIntegralFEM(plex,X,tt,ctx);CHKERRQ(ierr);
      momentum = tt[0]*ctx->masses[s_species_idx]/ctx->m_0;
      ierr = PetscDSSetObjective(prob, 0, &f0_re);CHKERRQ(ierr);
      ierr = DMPlexComputeIntegralFEM(plex,X,tt,ctx);CHKERRQ(ierr);
      energy = 0.5*tt[0]*ctx->masses[s_species_idx]/ctx->m_0;
      ierr = PetscDSSetObjective(prob, 0, &f0_rtherm);CHKERRQ(ierr);
      ierr = DMPlexComputeIntegralFEM(plex,X,tt,ctx);CHKERRQ(ierr);
      therm = tt[0]*ctx->masses[s_species_idx]/ctx->m_0;
      momentumtot += momentum;
      energytot  += energy;
      densitytot += density;
      thermtot   += therm;
      ierr = PetscPrintf(PETSC_COMM_WORLD, "%3D) species %D: charge density=%21.13e, momentum=%21.13e, energy=%21.13e, thermal=%21.13e\n",
			 stepi,s_species_idx,density,momentum,energy,therm);
      CHKERRQ(ierr);
    }
    ierr = DMPlexGetHeightStratum(serial_plex,0,&cStart,&cEnd);CHKERRQ(ierr);
    if (ctx->num_species>1) PetscPrintf(PETSC_COMM_WORLD, "\t%3D) Total normalized: charge density=%21.13e, momentum=%21.13e, energy=%21.13e, thermal flux=%21.13e (m_i[0]/m_e = %g, %D cells)\n",
					stepi,densitytot,momentumtot,energytot,thermtot,ctx->masses[1]/ctx->masses[0],cEnd-cStart);
    else PetscPrintf(PETSC_COMM_WORLD, "\t%3D) Normalized: charge density=%21.13e, momentum=%21.13e, energy=%21.13e, thermal flux=%21.13e, %D cells\n",stepi,densitytot,momentumtot,energytot,thermtot,cEnd-cStart);
    ierr = DMDestroy(&plex);CHKERRQ(ierr);
    ierr = DMDestroy(&serial_plex);CHKERRQ(ierr);
    /* plot */
    if (ctx->verbose > 2 && !ctx->sub_index) {
      ierr = LandDMVecView(X, stepi, ctx->plot_file_prefix);CHKERRQ(ierr);
    }
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LandTSView"
static PetscErrorCode LandTSView(TS ts)
{
  PetscErrorCode    ierr;
  DM                dm;
  LandCtx           *ctx;
  PetscInt          stepi;
  Vec               Xl;
  PetscFunctionBegin;
  ierr = TSGetApplicationContext(ts, &ctx);CHKERRQ(ierr);
  ierr = TSGetDM(ts, &dm);CHKERRQ(ierr);
  ierr = TSGetSolution(ts, &Xl);CHKERRQ(ierr);
  ierr = TSGetStepNumber(ts, &stepi);CHKERRQ(ierr);
  ierr = LandView(dm, Xl, ctx->current_step+stepi, ctx);CHKERRQ(ierr);
  {
    PetscInt    cStart, cEnd;
    DM          plex;
    ierr = DMConvert(ctx->serial_dm, DMPLEX, &plex);CHKERRQ(ierr);
    ierr = DMPlexGetHeightStratum(plex,0,&cStart,&cEnd);CHKERRQ(ierr);
    ierr = DMDestroy(&plex);CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "LandDMCreateMesh"
static PetscErrorCode LandDMCreateMesh(MPI_Comm comm, LandCtx *ctx, DM *dm)
{
  PetscErrorCode ierr;
  PetscReal      radius_minor = ctx->radius;
  PetscMPIInt    mpi_world_size;
  PetscFunctionBegin;
  ierr = MPI_Comm_size(comm, &mpi_world_size);CHKERRQ(ierr);
  if (ctx->use_cartesian) {
    PetscInt    cells[] = {1,2}, dimEmbed, nCoords, i, lo[] = {0,-radius_minor},hi[] = {radius_minor,radius_minor};
    Vec         coordinates;
    PetscScalar *coords;
    if (mpi_world_size > 2) {
      PetscReal size_2 = mpi_world_size/2.;
      if (mpi_world_size%2!=0) SETERRQ1(comm,PETSC_ERR_ARG_WRONG,"num processes (%D/2) not an integer",mpi_world_size);
      i = (PetscInt)(PetscSqrtReal(size_2));
      if (PetscPowReal((PetscReal)i,2.) != size_2) SETERRQ1(comm,PETSC_ERR_ARG_WRONG,"num processes (%D/2) not a square",mpi_world_size);
      cells[0] *= i; cells[1] *= i;
    }
    ierr = DMPlexCreateHexBoxMesh(comm, 2, cells, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, DM_BOUNDARY_NONE, dm);CHKERRQ(ierr);
    /* set domain size */
    ierr = DMGetCoordinatesLocal(*dm,&coordinates);CHKERRQ(ierr);
    ierr = DMGetCoordinateDim(*dm,&dimEmbed);CHKERRQ(ierr);
    ierr = VecGetLocalSize(coordinates,&nCoords);CHKERRQ(ierr);
    if (nCoords % dimEmbed) SETERRQ(PETSC_COMM_SELF,PETSC_ERR_ARG_SIZ,"Coordinate vector the wrong size");
    ierr = VecGetArray(coordinates,&coords);CHKERRQ(ierr);
    for (i = 0; i < nCoords; i += dimEmbed) {
      PetscInt j;
      PetscScalar *coord = &coords[i];
      for (j = 0; j < dimEmbed; j++) {
	coord[j] = lo[j] + coord[j] * (hi[j] - lo[j]);
      }
    }
    ierr = VecRestoreArray(coordinates,&coords);CHKERRQ(ierr);
    ierr = DMSetCoordinatesLocal(*dm,coordinates);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject) *dm, "half-plane");CHKERRQ(ierr);
  } else {
    PetscInt       numCells = 4;
    PetscInt       numVerts = 8;
    PetscReal      inner_mult = ctx->inner_mult;
    double         *flatCoords = NULL;
    int            cells[4][4] = {{0,1,5,4},
				  {1,2,6,5},
				  {2,3,7,6},
				  {4,5,6,7}};
    int            *flatCells = &cells[0][0],*pcell;
    if (mpi_world_size > 4) SETERRQ1(comm,PETSC_ERR_ARG_WRONG,"too may processes %D > 4",mpi_world_size);
    ierr = PetscMalloc2(numVerts * 2, &flatCoords, numCells * 4, &flatCells);CHKERRQ(ierr);
    {
      double (*coords)[2] = (double (*) [2]) flatCoords;
      PetscInt j;
      for (j = 0; j < 8; j++) {
	double z,r;
	double mult = (j >= 4) ? inner_mult : 1., theta = -M_PI_2 + (j%4) * M_PI/3;
	z = mult * radius_minor * sin(theta);
	coords[j][1] = z;
	r = mult * radius_minor * cos(theta);
	coords[j][0] = r;
      }
      for (j = 0, pcell = flatCells; j < 4; j++, pcell += 4) {
	pcell[0] = cells[j][0]; pcell[1] = cells[j][1];
	pcell[2] = cells[j][2]; pcell[3] = cells[j][3];
      }	  
    }
    ierr = DMPlexCreateFromCellList(comm,2,numCells,numVerts,4,ctx->interpolate,flatCells,2,flatCoords,dm);CHKERRQ(ierr);
    ierr = PetscFree2(flatCoords,flatCells);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject) *dm, "semi-circle");CHKERRQ(ierr);
  }
  PetscFunctionReturn(0);
}

#define MATVEC2(__a,__x,__p) {int i,j; for (i=0.; i<2; i++) {__p[i] = 0; for (j=0.; j<2; j++) __p[i] += __a[i][j]*__x[j]; }}
static void LandCircleInflate(const PetscReal radius, const PetscReal inner_mult, const PetscReal x, const PetscReal y,
			    PetscReal *outX, PetscReal *outY)
{
  PetscReal rr = PetscSqrtReal(x*x + y*y);
  if (rr < 1.e-12) {
    *outX = x; *outY = y; /* inside box, just use default refinement */

  } else {
    const PetscReal xy[2] = {x,y}, inner_x = 0.866025403784439*inner_mult*radius, sinphi=y/rr, cosphi=x/rr;
    PetscReal cth,sth,xyprime[2],Rth[2][2];
    /* rotate normalized vector into [-pi/6,pi/6) */
    if (sinphi >= 0.5) {         /* top cell, -pi/3 */
      cth = 0.5; sth = -0.866025403784439;
    } else if (sinphi >= -0.5) { /* mid cell + 0 */
      cth = 1.; sth = 0.;
    } else {                     /* bottom cell + pi/3 */
      cth = 0.5; sth = .866025403784439;
    }
    Rth[0][0] = cth; Rth[0][1] =-sth;
    Rth[1][0] = sth; Rth[1][1] = cth;
    MATVEC2(Rth,xy,xyprime);
    assert(xyprime[0] > .86602540378443*rr); /* rotate into [-pi/6,pi/6) */
    assert(PetscAbsReal(xyprime[1]) < 0.500000001*rr);
    if (xyprime[0] < inner_x + 1.e-12) {
      *outX = x; *outY = y; /* inside box, just use default refinement */
    } else {
      PetscReal ncosphi=xyprime[0]/rr, rin = inner_x/ncosphi, rout = rr - rin;
      PetscReal routmax=radius*0.866025403784439/ncosphi - rin, nroutmax = radius - rin, routfrac = rout/routmax; assert(routfrac > -1.e-12 && routfrac < 1.000001); assert(routmax<nroutmax+1.e-12);
      PetscReal nrr = rin + routfrac*nroutmax;
      *outX = cosphi*nrr; *outY = sinphi*nrr;
    }
  }
}

#undef __FUNCT__
#define __FUNCT__ "GeometryDMLandau"
static PetscErrorCode GeometryDMLandau(DM base, PetscInt point, PetscInt dim, const PetscReal ab[], PetscReal xy[], void *actx)
{
  const LandCtx *ctx = (LandCtx*)actx;
  PetscReal x, y;
  PetscFunctionBegin;
  x = ab[0];
  y = ab[1];
  LandCircleInflate(ctx->radius,ctx->inner_mult,x,y,&x,&y);
  xy[0] = x;
  xy[1] = y;
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "ErrorIndicator_Simple"
static PetscErrorCode ErrorIndicator_Simple(PetscInt dim, PetscReal volume, PetscInt Nf, const PetscInt Nc[], const PetscScalar u[], const PetscScalar grad[], PetscReal *error, void *actx)
{
  LandCtx *ctx = (LandCtx*)actx;
  PetscReal err = 0.0;
  PetscInt  f, j;
  PetscFunctionBeginUser;
  for (f = 0; f < Nf; ++f) {
    /* f = Nf-1; */
    for (j = 0; j < dim; ++j) {
      err += PetscSqr(PetscRealPart(grad[f*dim+j])) * ctx->amr_factors[f];
    }
    /* err += PetscSqr(PetscRealPart(u[f])) * afact[f]; */
  }
  *error = volume * err;
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "initializeTS"
static PetscErrorCode initializeTS(DM dm, LandCtx *ctx, TS *ts)
{
  PetscErrorCode ierr;
  SNES           snes;
  SNESLineSearch linesearch; /* line search */
  PetscFunctionBegin;
  ierr = TSCreate(PetscObjectComm((PetscObject)dm), ts);CHKERRQ(ierr);
  ierr = TSSetType(*ts, TSSSP);CHKERRQ(ierr);
  ierr = TSSetDM(*ts, dm);CHKERRQ(ierr);
  ierr = DMTSSetBoundaryLocal(dm, DMPlexTSComputeBoundary, ctx);CHKERRQ(ierr);
  ierr = TSSetTimeStep(*ts,1.e-5);CHKERRQ(ierr);
  ierr = TSSetMaxSteps(*ts, 5);CHKERRQ(ierr);
  ierr = TSSetMaxTime(*ts, 1.e1);CHKERRQ(ierr);
  ierr = TSSetExactFinalTime(*ts,TS_EXACTFINALTIME_STEPOVER);CHKERRQ(ierr);
  ierr = DMSetApplicationContext(dm, ctx);CHKERRQ(ierr); /* side effect! */
  ierr = TSSetApplicationContext(*ts, ctx);CHKERRQ(ierr);
  ierr = TSSetPostStep(*ts, LandTSView);CHKERRQ(ierr);
  ierr = TSGetSNES(*ts,&snes);CHKERRQ(ierr);
  ierr = SNESGetLineSearch(snes,&linesearch);CHKERRQ(ierr);
  ierr = SNESLineSearchSetType(linesearch,SNESLINESEARCHBASIC);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "adaptToleranceFEM"
static PetscErrorCode adaptToleranceFEM(PetscFE fem[], TS ts, Vec sol, PetscReal refineTol, PetscReal coarsenTol, LandCtx *ctx, TS *tsNew, Vec *solNew)
{
  DM               dm, plex, adaptedDM = NULL;
  PetscDS          prob;
  PetscSection     section;
  PetscBool        isForest;
  Vec              locX;
  PetscQuadrature  quad;
  const PetscReal *quadPoints, *quadWeights;
  PetscInt         Nq, Nf, *Nb, *Nc, cStart, cEnd, cEndInterior, c, dim, qj;
  PetscReal      **B, **D, *x;
  PetscScalar     *refSpaceDer, *u, *u_x;
  PetscReal        minMaxInd[2] = {PETSC_MAX_REAL, PETSC_MIN_REAL}, minMaxIndGlobal[2], minInd, maxInd, time;
  DMLabel          adaptLabel = NULL;
  PetscErrorCode   ierr;
  PetscFunctionBegin;
#if defined(PETSC_USE_LOG)
  ierr = PetscLogEventBegin(ctx->events[9],0,0,0,0);CHKERRQ(ierr);
#endif
  ierr = TSGetTime(ts, &time);CHKERRQ(ierr);
  ierr = VecGetDM(sol, &dm);CHKERRQ(ierr);
  ierr = DMGetDS(dm, &prob);CHKERRQ(ierr);
  ierr = DMGetDimension(dm, &dim);CHKERRQ(ierr);
  ierr = DMGetDefaultSection(dm, &section);CHKERRQ(ierr);
  ierr = DMIsForest(dm, &isForest);CHKERRQ(ierr);
  ierr = DMConvert(dm, DMPLEX, &plex);CHKERRQ(ierr);
  ierr = DMCreateLocalVector(plex, &locX);CHKERRQ(ierr);
  ierr = DMPlexInsertBoundaryValues(plex, PETSC_TRUE, locX, time, NULL, NULL, NULL);CHKERRQ(ierr);
  ierr = DMGlobalToLocalBegin(plex, sol, INSERT_VALUES, locX);CHKERRQ(ierr);
  ierr = DMGlobalToLocalEnd  (plex, sol, INSERT_VALUES, locX);CHKERRQ(ierr);
  ierr = DMPlexGetHeightStratum(plex,0,&cStart,&cEnd);CHKERRQ(ierr);
  ierr = DMPlexGetHybridBounds(plex,&cEndInterior,NULL,NULL,NULL);CHKERRQ(ierr);
  cEnd = (cEndInterior < 0) ? cEnd : cEndInterior;
  ierr = DMLabelCreate("adapt",&adaptLabel);CHKERRQ(ierr);
  ierr = PetscFEGetQuadrature(fem[0], &quad);CHKERRQ(ierr);
  ierr = PetscQuadratureGetData(quad, NULL, NULL, &Nq, &quadPoints, &quadWeights);CHKERRQ(ierr);
  ierr = PetscDSGetTabulation(prob, &B, &D);CHKERRQ(ierr);
  ierr = PetscDSGetNumFields(prob, &Nf);CHKERRQ(ierr);
  ierr = PetscDSGetDimensions(prob, &Nb);CHKERRQ(ierr);
  ierr = PetscDSGetComponents(prob, &Nc);CHKERRQ(ierr);
  ierr = PetscDSGetEvaluationArrays(prob, &u, NULL, &u_x);CHKERRQ(ierr);
  ierr = PetscDSGetRefCoordArrays(prob, &x, &refSpaceDer);CHKERRQ(ierr);
  for (c = cStart; c < cEnd; c++) {
    PetscScalar *coef = NULL;
    PetscReal    vol, errInd = 0., tt;
    PetscReal    v0[3], J[9], invJ[9], detJ;

    ierr = DMPlexComputeCellGeometryFVM(plex, c, &vol, NULL, NULL);CHKERRQ(ierr);
    ierr = DMPlexComputeCellGeometryFEM(plex, c, NULL, v0, J, invJ, &detJ);CHKERRQ(ierr);
    ierr = DMPlexVecGetClosure(plex, section, locX, c, NULL, &coef);CHKERRQ(ierr);
    for (qj = 0; qj < Nq; ++qj) {
      EvaluateFieldJets(dim, Nf, Nb, Nc, qj, B, D, refSpaceDer, invJ, coef, NULL, u, u_x, NULL);
      ierr = (ctx->errorIndicator)(dim, vol, Nf, Nc, u, u_x, &tt, ctx);CHKERRQ(ierr);
      errInd += tt;
    }
    ierr = DMPlexVecRestoreClosure(plex, section, locX, c, NULL, &coef);CHKERRQ(ierr);
    minMaxInd[0] = PetscMin(minMaxInd[0], errInd);
    minMaxInd[1] = PetscMax(minMaxInd[1], errInd);
    if (errInd > refineTol)  {ierr = DMLabelSetValue(adaptLabel, c, DM_ADAPT_REFINE);CHKERRQ(ierr);}
    if (errInd < coarsenTol) {ierr = DMLabelSetValue(adaptLabel, c, DM_ADAPT_COARSEN);CHKERRQ(ierr);}
  }
  ierr = VecDestroy(&locX);CHKERRQ(ierr);
  ierr = DMDestroy(&plex);CHKERRQ(ierr);
  minMaxInd[1] = -minMaxInd[1];
  ierr = MPI_Allreduce(minMaxInd,minMaxIndGlobal,2,MPIU_REAL,MPI_MIN,PetscObjectComm((PetscObject)dm));CHKERRQ(ierr);
  minInd = minMaxIndGlobal[0];
  maxInd = -minMaxIndGlobal[1];
  ierr = PetscInfo2(ts, "error indicator range (%E, %E)\n", minInd, maxInd);CHKERRQ(ierr);
  if (maxInd > refineTol || minInd < coarsenTol) { /* at least one cell is over the refinement threshold */
    ierr = DMAdaptLabel(dm, adaptLabel, &adaptedDM);CHKERRQ(ierr);
  } else if (maxInd < coarsenTol) { /* all cells are under the coarsening threshold */
    ierr = DMCoarsen(dm, PetscObjectComm((PetscObject) dm), &adaptedDM);CHKERRQ(ierr);
  } else if (minInd > refineTol) { /* all cells are over the refinement threshold */
    ierr = DMRefine(dm, PetscObjectComm((PetscObject) dm), &adaptedDM);CHKERRQ(ierr);
  }
  ierr = DMLabelDestroy(&adaptLabel);CHKERRQ(ierr);
  if (adaptedDM) {
    if (tsNew) {
      ierr = initializeTS(adaptedDM,ctx,tsNew);CHKERRQ(ierr);
    }
    if (solNew) {
      PetscObject fe;
      char        buf[256];
      int         ii;
      /* fix name for plotting, gets lost in refinement */
      ierr = DMGetDS(adaptedDM, &prob);CHKERRQ(ierr);
      for (ii=0;ii<ctx->num_species;ii++) {
	if (ii==0) ierr = PetscSNPrintf(buf, 256, "e");
	else ierr = PetscSNPrintf(buf, 256, "i%D", ii);
	CHKERRQ(ierr);
	ierr = PetscDSGetDiscretization(prob, ii, &fe);CHKERRQ(ierr);
	ierr = PetscObjectSetName(fe, buf);CHKERRQ(ierr);
      }
      /* make new solution vector */
      ierr = DMCreateGlobalVector(adaptedDM, solNew);CHKERRQ(ierr);
      ierr = PetscObjectSetName((PetscObject) *solNew, "u");CHKERRQ(ierr);
      ierr = DMForestTransferVec(dm, sol, adaptedDM, *solNew, PETSC_TRUE, time);CHKERRQ(ierr);
    }
    if (isForest) {ierr = DMForestSetAdaptivityForest(adaptedDM,NULL);CHKERRQ(ierr);} /* clear internal references to the previous dm */
    ierr = DMDestroy(&adaptedDM);CHKERRQ(ierr);
  } else {
    if (tsNew) *tsNew = NULL;
    if (solNew) *solNew = NULL;
  }
#if defined(PETSC_USE_LOG)
  ierr = PetscLogEventEnd(ctx->events[9],0,0,0,0);CHKERRQ(ierr);
#endif
  PetscFunctionReturn(0);
}

#undef __FUNCT__
#define __FUNCT__ "SetInitialCondition"
PetscErrorCode SetInitialCondition(DM dm, Vec X, LandCtx *ctx)
{
  PetscErrorCode (*initu[LAND_NUM_SPECIES_MAX])(PetscInt, PetscReal, const PetscReal [], PetscInt, PetscScalar [], void *);
  PetscErrorCode ierr,ii;
  PetscReal *array[LAND_NUM_SPECIES_MAX],data[3*LAND_NUM_SPECIES_MAX], ioncharge = 0;
  PetscFunctionBeginUser;
  ioncharge = ctx->charges[1]; /* assume rest have small scaling */
  for (ii=0;ii<ctx->num_species;ii++) {
    array[ii] = &data[3*ii];
    data[3*ii] = 2*ctx->thermal_temps[ii]/ctx->masses[ii]*ctx->m_0; /* data=2T/mc^2 */
    data[3*ii+1] = ii==0 ? 1. : ctx->charges[ii]/ioncharge; /* scale rho */
    data[3*ii+2] = 0; /* shift */
    if (ii>1) {
      data[3*ii+1] *= 1.e-4; /* scale rho down */
    }
    initu[ii] = bimaxwellian;
  }
  data[2] = -.5; /* electron shift */
  ierr = DMProjectFunction(dm, 0.0, initu, (void**)array, INSERT_ALL_VALUES, X);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

/* ------------------------------------------------------------------- */
#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc,char **argv)
{
  TS              ts;
  Vec             uu;  // the solution vector and residual
  PetscErrorCode  ierr;
  LandCtx         actx, *ctx = &actx;
  PetscInt        dim=2,Nelem,Neq,Nq,totDim,ii,sub_mpi_size;
  size_t          len;
  PetscDS         prob;
  PetscInt        targetCells = 0, adaptInterval;
  PetscMPIInt     rank,mpi_world_size;
  PetscBool       tensor = PETSC_FALSE, all_mpi = PETSC_FALSE, flg;
  Mat             J;
  PetscReal       refineTol,coarsenTol;
  PetscQuadrature quad;
  DM              dm;
  PetscFunctionBegin;
  /* Initialize PETSC */
  ierr = PetscInitialize(&argc,&argv,(char*)0,help); if (ierr) return ierr;
  ierr = MPI_Comm_rank(PETSC_COMM_WORLD, &rank);CHKERRQ(ierr);
  ierr = MPI_Comm_size(PETSC_COMM_WORLD, &mpi_world_size);CHKERRQ(ierr);
  ierr = PetscMemzero(&actx,sizeof(actx));CHKERRQ(ierr);
  /* get options - initialize context */
  ctx->interpolate = PETSC_TRUE;
  ctx->use_cartesian = PETSC_TRUE;
  ctx->use_amr = PETSC_FALSE;
  ctx->matrix_free_residual = PETSC_FALSE;
  ctx->errorIndicator = NULL;
  ctx->current_step = 0;
  ctx->filename[0] = '\0';
  ctx->Ez = 1.e-15;
  ctx->radius = 2;
  ctx->inner_mult = 0.5;
  ctx->num_species = 1;
  ctx->num_min_cells = 100;
  /* hardwired and derived physics */
  ctx->epsilon0 = 8.8542e-12; /* permittivity of free space (MKS) */
  /* ctx->k=1.38064852e-23; */ /* Boltzmann constant (MKS) */
  ctx->charges[0] = -1.6022e-19;  /* electron charge (MKS) */
  ctx->charges[1] = -ctx->charges[0];  /* ions [i+] */
  ctx->charges[2] = -2.*ctx->charges[0];  /* alpha */
  ctx->masses[1] = 1.6720e-27; /* proton mass kg */
  ctx->masses[2] = 4.*ctx->masses[1]; /* alpha mass */
  ctx->masses[0] = ctx->masses[1]/1835.5; /* electron mass */
  /* local var parameters */
  refineTol     = PETSC_MAX_REAL;
  coarsenTol    = 0.;
  adaptInterval = 1;
  ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "", "Options for Fokker-Plank-Landau collision operator", "none");CHKERRQ(ierr);
  ierr = PetscOptionsBool("-interpolate", "interpolate grid points in refinement", "main.c", ctx->interpolate, &ctx->interpolate, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-all_mpi", "Use all processors for one solve", "main.c", all_mpi, &all_mpi, NULL);CHKERRQ(ierr);
  /* ierr = PetscOptionsBool("-use_cartesian", "use simple Cartesian grid for p4est instead of semi-circle", "main.c", ctx->use_cartesian, &ctx->use_cartesian, NULL);CHKERRQ(ierr); */
  ierr = PetscOptionsBool("-use_amr", "use adaptive mesh refinement with p4est", "main.c", ctx->use_amr, &ctx->use_amr, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-matrix_free_residual", "use simple Cartesian grid for p4est instead of semi-circle", "main.c", ctx->matrix_free_residual, &ctx->matrix_free_residual, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_min_cells", "Number of global cells to try to maintain", "main.c", ctx->num_min_cells, &ctx->num_min_cells, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_species", "Number of species", "main.c", ctx->num_species, &ctx->num_species, NULL);CHKERRQ(ierr);
  if (ctx->num_species > LAND_NUM_SPECIES_MAX) SETERRQ1(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"too many species %D",ctx->num_species);
  ierr = PetscOptionsInt("-verbose", "Verbose level", "main.c", ctx->verbose, &ctx->verbose, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsBool("-petscspace_poly_tensor", "", "main.c", tensor, &tensor, NULL);CHKERRQ(ierr);
  ctx->simplex = tensor ? PETSC_FALSE : PETSC_TRUE;
  ii = LAND_NUM_SPECIES_MAX;
  ierr = PetscOptionsRealArray("-thermal_temps", "Temperature of each species [e,i_0,i_1,...] <in keV>", "main.c", ctx->thermal_temps, &ii, NULL);CHKERRQ(ierr);
  while (ii < LAND_NUM_SPECIES_MAX) ctx->thermal_temps[ii++] = 0.01;
  /* convert to Kelvin */
  /* for (ii=0;ii<ctx->num_species;ii++) ctx->thermal_temps[ii] *= 1.16e7; */
  ierr = PetscOptionsReal("-Ez", "External electric field", "main.c", ctx->Ez, &ctx->Ez, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsString("-f", "The mesh file", "main.c", ctx->filename, ctx->filename, PETSC_MAX_PATH_LEN, NULL);CHKERRQ(ierr);
  ierr = PetscSNPrintf(ctx->plot_file_prefix, PETSC_MAX_PATH_LEN, "u");CHKERRQ(ierr);
  ierr = PetscOptionsString("-plot_file_prefix", "Prefix for plot files", "main.c", ctx->plot_file_prefix, ctx->plot_file_prefix, 16, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-refine_tol","tolerance for refining cells in AMR","",refineTol,&refineTol,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsReal("-coarsen_tol","tolerance for coarsening cells in AMR","",coarsenTol,&coarsenTol,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-adapt_interval","time steps between AMR","",adaptInterval,&adaptInterval,NULL);CHKERRQ(ierr);
  ii = LAND_NUM_SPECIES_MAX;
  ierr = PetscOptionsRealArray("-masses", "Mass of each species in units of electron mass [e=1,i_0=1835.5,i_1=7342,...]", "main.c", ctx->masses, &ii, &flg);CHKERRQ(ierr);
  while (flg && ii--) {
    ctx->masses[ii] *= 9.10938356e-31; /* electron mass kg */
    if (ctx->masses[ii] <= 0.0) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"Negative mass[%D] %g",ii,ctx->masses[ii]);
  }
  ctx->m_0 = ctx->masses[0]; /* arbitrary reference mass, electrons */
  ii = LAND_NUM_SPECIES_MAX;
  ierr = PetscOptionsRealArray("-charges", "Charge of each species in units of proton charge [e=-1,i_0=1,i_1=2,...]", "main.c", ctx->charges, &ii, &flg);CHKERRQ(ierr);
  while (flg && ii--) ctx->charges[ii] *= 1.6022e-19; /* electron charge (MKS) */
  sub_mpi_size = 1;
  ierr = PetscOptionsInt("-sub_mpi_size", "Number of MPI ranks per subdomain", "main.c", sub_mpi_size, &sub_mpi_size, NULL);CHKERRQ(ierr);
  if (mpi_world_size==1) sub_mpi_size=1; /* just fix this common thing */
  else if (all_mpi) sub_mpi_size = mpi_world_size;
  if (sub_mpi_size==1) {
    ctx->comm = PETSC_COMM_SELF;
    ctx->sub_rank = 0;
    ctx->sub_index = rank;
  } else {
    if (mpi_world_size%sub_mpi_size || sub_mpi_size>mpi_world_size) SETERRQ2(PETSC_COMM_SELF,PETSC_ERR_ARG_WRONG,"bad communicator sizes %D %D",sub_mpi_size,mpi_world_size);
    else if (sub_mpi_size == mpi_world_size) {
      ctx->comm = PETSC_COMM_WORLD;
      ctx->sub_rank = rank;
      ctx->sub_index = 0;
    } else {
      PetscMPIInt new_size;
      ctx->sub_rank = rank%sub_mpi_size;
      ctx->sub_index = rank/sub_mpi_size;
      ierr = MPI_Comm_split(PETSC_COMM_WORLD, ctx->sub_index, ctx->sub_rank, &ctx->comm);CHKERRQ(ierr);
      ierr = MPI_Comm_size(ctx->comm, &new_size);CHKERRQ(ierr);
      if (new_size != sub_mpi_size) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_ARG_WRONG,"size is wrong ???");
    }
  }
  ierr = PetscOptionsEnd();
  {
    PetscInt currevent = 0;
    /* PetscLogStage  setup_stage; */
    ierr = PetscLogEventRegister("Landau Operator", DM_CLASSID, &ctx->events[currevent++]);CHKERRQ(ierr); /* 0 */
    ierr = PetscLogEventRegister(" Jacobian-setup", DM_CLASSID, &ctx->events[currevent++]);CHKERRQ(ierr); /* 1 */
    ierr = PetscLogEventRegister(" Jacobian-geom", DM_CLASSID, &ctx->events[currevent++]);CHKERRQ(ierr); /* 2 */
    ierr = PetscLogEventRegister(" Jacobian-kern-i", DM_CLASSID, &ctx->events[currevent++]);CHKERRQ(ierr); /* 3 */
    ierr = PetscLogEventRegister(" Jacobian-kernel", DM_CLASSID, &ctx->events[currevent++]);CHKERRQ(ierr); /* 4 */
    ierr = PetscLogEventRegister(" Jacobian-trans", DM_CLASSID, &ctx->events[currevent++]);CHKERRQ(ierr); /* 5 */
    ierr = PetscLogEventRegister(" Jacobian-assem", DM_CLASSID, &ctx->events[currevent++]);CHKERRQ(ierr); /* 6 */
    ierr = PetscLogEventRegister(" Jacobian-end", DM_CLASSID, &ctx->events[currevent++]);CHKERRQ(ierr); /* 7 */
    ierr = PetscLogEventRegister("Landau Jacobian", DM_CLASSID, &ctx->events[currevent++]);CHKERRQ(ierr); /* 8 */
    ierr = PetscLogEventRegister("Forest", DM_CLASSID, &ctx->events[currevent++]);CHKERRQ(ierr); /* 9 */
     if (ctx->sub_index) { /* turn off output stuff for duplicate runs */
      ierr = PetscOptionsClearValue(NULL,"-snes_converged_reason");CHKERRQ(ierr);
      ierr = PetscOptionsClearValue(NULL,"-ksp_converged_reason");CHKERRQ(ierr);
      ierr = PetscOptionsClearValue(NULL,"-snes_monitor");CHKERRQ(ierr);
      ierr = PetscOptionsClearValue(NULL,"-ksp_monitor");CHKERRQ(ierr);
      ierr = PetscOptionsClearValue(NULL,"-ts_monitor");CHKERRQ(ierr);
      ierr = PetscOptionsClearValue(NULL,"-dm_view");CHKERRQ(ierr);
      ierr = PetscOptionsClearValue(NULL,"-vec_view");CHKERRQ(ierr);
    }
  }

  /* hack to get refinement good */
  for (ii=0;ii<ctx->num_species;ii++) ctx->amr_factors[ii] = 1;
  ctx->amr_factors[0] = 3e+12;

  /* Create Mesh */
  ierr = PetscStrlen(ctx->filename, &len);CHKERRQ(ierr);
  if (!len) { /* p4est, quads */
    /* Create plex mesh of Landau domain */
    ierr = LandDMCreateMesh(ctx->comm, ctx, &ctx->dm);CHKERRQ(ierr);
  } else {
    ierr = DMPlexCreateFromFile(ctx->comm, ctx->filename, ctx->interpolate, &ctx->dm);CHKERRQ(ierr);
    ierr = DMGetDimension(ctx->dm, &dim);CHKERRQ(ierr);
    if (dim != 2) SETERRQ1(PETSC_COMM_WORLD, PETSC_ERR_ARG_WRONG, "dim %D != 2", dim);
  }
#if defined(LAND_ADD_BCS)
#error
  if (1) { /* mark BCs */
    DMLabel        label;
    PetscInt       fStart, fEnd, f;
    ierr = DMCreateLabel(ctx->dm, "marker");CHKERRQ(ierr);
    ierr = DMGetLabel(ctx->dm, "marker", &label);CHKERRQ(ierr);
    ierr = DMPlexGetHeightStratum(ctx->dm, 1, &fStart, &fEnd);CHKERRQ(ierr);
    for (f = fStart; f < fEnd; ++f) {
      PetscInt supportSize;
      ierr = DMPlexGetSupportSize(ctx->dm, f, &supportSize);CHKERRQ(ierr);
      if (supportSize == 1) {
	PetscReal c[3];
	ierr = DMPlexComputeCellGeometryFVM(ctx->dm, f, NULL, c, NULL);CHKERRQ(ierr);
	if (PetscAbsReal(c[0]) >1.e-12) {
	  ierr = DMLabelSetValue(label, f, 1);CHKERRQ(ierr);
	}
      }
    }
    ierr = DMPlexLabelComplete(ctx->dm, label);CHKERRQ(ierr);
  }
#endif
  /* distribute */
  ierr = DMPlexDistribute(ctx->dm, 0, NULL, &dm);CHKERRQ(ierr);
  if (dm) {
    ierr = DMDestroy(&ctx->dm);CHKERRQ(ierr);
    ctx->dm = dm;
  }
  { /* p4est? */
    char convType[256];
    PetscBool flg;
    ierr = PetscOptionsBegin(PETSC_COMM_WORLD, "", "Mesh conversion options", "DMPLEX");CHKERRQ(ierr);
    ierr = PetscOptionsFList("-dm_type","Convert DMPlex to another format (should not be Plex!)","ex1.c",DMList,DMPLEX,convType,256,&flg);CHKERRQ(ierr);
    ierr = PetscOptionsEnd();
    if (flg) {
      DM dmforest;
      ierr = DMConvert(ctx->dm,convType,&dmforest);CHKERRQ(ierr);
      if (dmforest) {
        const char *prefix;
        PetscBool isForest;
        ierr = PetscObjectGetOptionsPrefix((PetscObject)ctx->dm,&prefix);CHKERRQ(ierr);
        ierr = PetscObjectSetOptionsPrefix((PetscObject)dmforest,prefix);CHKERRQ(ierr);
        ierr = DMIsForest(dmforest,&isForest);CHKERRQ(ierr);
        if (isForest) {
          if (!ctx->use_cartesian) {
            ierr = DMForestSetBaseCoordinateMapping(dmforest,GeometryDMLandau,ctx);CHKERRQ(ierr);
          }
	  ierr = DMDestroy(&ctx->dm);CHKERRQ(ierr);
	  ctx->dm = dmforest;
          ctx->errorIndicator = ErrorIndicator_Simple;
        } else SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Converted to non Forest?");
      } else SETERRQ(PETSC_COMM_WORLD, PETSC_ERR_USER, "Convert failed?");
    } else {
      ctx->use_amr = PETSC_FALSE;
      ctx->num_min_cells = 0;
    }
  }
  ierr = DMPlexGetRedundantDM(ctx->dm, &ctx->serial_dm);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) ctx->dm, "Mesh");CHKERRQ(ierr);
  ierr = DMSetFromOptions(ctx->dm);CHKERRQ(ierr); /* refine */
  ierr = DMGetDS(ctx->dm, &prob);CHKERRQ(ierr);
  if (ctx->dm != ctx->serial_dm) {
    ierr = PetscObjectSetName((PetscObject) ctx->serial_dm, "Serial Mesh");CHKERRQ(ierr);
    ierr = DMSetFromOptions(ctx->serial_dm);CHKERRQ(ierr); /* refine */
    ierr = DMSetDS(ctx->serial_dm, prob);CHKERRQ(ierr);
  }
  /* FEM prob */
  for (ii=0;ii<ctx->num_species;ii++) {
    char     buf[256];
    if (ii==0) ierr = PetscSNPrintf(buf, 256, "e");
    else ierr = PetscSNPrintf(buf, 256, "i%D", ii);
    CHKERRQ(ierr);
    /* Setup Discretization - FEM */
    ierr = PetscFECreateDefault(ctx->dm, dim, 1, ctx->simplex, NULL, PETSC_DECIDE, &ctx->fe[ii]);CHKERRQ(ierr);
    ierr = LandFEChangeQuadrature(ctx->fe[ii]);CHKERRQ(ierr);
    ierr = PetscObjectSetName((PetscObject) ctx->fe[ii], buf);CHKERRQ(ierr);
#if defined(LAND_ADD_BCS)
    PetscInt id=1;
    ierr = PetscDSAddBoundary(prob, DM_BC_ESSENTIAL, "wall", "marker", ii, 0, NULL, (void (*)()) zero, 1, &id, ctx);CHKERRQ(ierr);
#endif
    ierr = PetscDSSetDiscretization(prob, ii, (PetscObject) ctx->fe[ii]);CHKERRQ(ierr); /* this is where Nf gets set */
  }
  ierr = PetscFEGetQuadrature(ctx->fe[0], &quad);CHKERRQ(ierr);
  ierr = PetscQuadratureGetData(quad, NULL, NULL, &Nq, NULL, NULL);CHKERRQ(ierr); /* Nq are all the same */
  ierr = PetscDSGetTotalDimension(prob, &totDim);CHKERRQ(ierr);
  /* create ts */
  ierr = initializeTS(ctx->dm,ctx,&ts);CHKERRQ(ierr);
  /* set initial state */
  ierr = DMCreateGlobalVector(ctx->dm,&uu);CHKERRQ(ierr);
  ierr = VecGetSize(uu, &Neq);CHKERRQ(ierr);
  ierr = PetscObjectSetName((PetscObject) uu, "u");CHKERRQ(ierr);
  ierr = SetInitialCondition(ctx->dm, uu, ctx);CHKERRQ(ierr);
  /* initial static refinement, no solve */
  if (ctx->errorIndicator) {
    PetscInt adaptIter;
    if (targetCells > 0) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"AMR with target cell count not implemented yet.");
    for (adaptIter = 0;;adaptIter++) {
      PetscLogDouble bytes;
      TS             tsNew = NULL;
      if (ctx->verbose > 2 && !ctx->sub_index) {
	char              buf[256];
	ierr = PetscSNPrintf(buf, 256, "%s-initial", ctx->plot_file_prefix);CHKERRQ(ierr);
	ierr = LandDMVecView(uu, adaptIter, buf);CHKERRQ(ierr);
      }
      ierr = PetscMemoryGetCurrentUsage(&bytes);CHKERRQ(ierr);
      ierr = PetscInfo2(ts, "refinement loop %D: memory used %g\n", adaptIter, bytes);CHKERRQ(ierr);
      ierr = adaptToleranceFEM(ctx->fe, ts, uu, refineTol, coarsenTol, ctx, &tsNew, NULL);CHKERRQ(ierr);
      if (!tsNew) {
        break;
      } else {
	ierr = PetscInfo(ts, "AMR used\n");CHKERRQ(ierr);
        ierr = DMDestroy(&ctx->dm);CHKERRQ(ierr);
        ierr = DMDestroy(&ctx->serial_dm);CHKERRQ(ierr);
        ierr = VecDestroy(&uu);CHKERRQ(ierr);
        ierr = TSDestroy(&ts);CHKERRQ(ierr);
        ts   = tsNew;
        ierr = TSGetDM(ts, &ctx->dm);CHKERRQ(ierr);
        ierr = DMPlexGetRedundantDM(ctx->dm, &ctx->serial_dm);CHKERRQ(ierr);
        ierr = PetscObjectReference((PetscObject)ctx->dm);CHKERRQ(ierr);
	ierr = DMCreateGlobalVector(ctx->dm,&uu);CHKERRQ(ierr);
	ierr = PetscObjectSetName((PetscObject) uu, "u");CHKERRQ(ierr);
        ierr = SetInitialCondition(ctx->dm, uu, ctx);CHKERRQ(ierr);
      }
    }
  }
  /* setup mesh data and solver */
  ierr = DMCreateMatrix(ctx->dm, &ctx->J);CHKERRQ(ierr);
  ierr = DMCreateMatrix(ctx->dm, &J);CHKERRQ(ierr);
  ierr = LandCreateMassMatrix(ctx,uu,&ctx->M);CHKERRQ(ierr);
  ierr = TSSetIFunction(ts,NULL,LandIFunction,ctx);CHKERRQ(ierr);
  ierr = TSSetIJacobian(ts,J,J,LandIJacobian,ctx);CHKERRQ(ierr);
  ierr = DMViewFromOptions(ctx->dm,NULL,"-dm_view");CHKERRQ(ierr);
  ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
  /* solve */
  ierr = TSSetSolution(ts,uu);CHKERRQ(ierr);
  ierr = TSSetUp(ts);CHKERRQ(ierr);
  ierr = LandView(ctx->dm, uu, 0, ctx);CHKERRQ(ierr); /* print initial state like LandTSView */
  MPI_Barrier(MPI_COMM_WORLD);
  if (!(ctx->errorIndicator && ctx->use_amr)) {
    ierr = TSSolve(ts,uu);
    if (ierr) PetscPrintf(PETSC_COMM_WORLD, "TSSolve failed\n");
    ierr = TSGetSolveTime(ts,&ctx->current_time);CHKERRQ(ierr);
    ierr = TSGetStepNumber(ts, &ctx->current_step);CHKERRQ(ierr);
  } else {
    TS        tsNew  = NULL;
    Vec       solNew = NULL;
    PetscReal finalTime;
    PetscInt  adaptIter,finalStep;
    ierr   = TSGetMaxTime(ts, &finalTime);CHKERRQ(ierr);
    ierr   = TSGetMaxSteps(ts, &finalStep);CHKERRQ(ierr);
    ierr   = TSSetMaxSteps(ts, adaptInterval);CHKERRQ(ierr);
    ierr   = TSSolve(ts, uu);CHKERRQ(ierr); /* initial advance adaptInterval steps */
    ierr   = TSGetSolveTime(ts, &ctx->current_time);CHKERRQ(ierr);
    ierr   = TSGetStepNumber(ts, &ctx->current_step);CHKERRQ(ierr);
    for (adaptIter = 0; ctx->current_time < finalTime && ctx->current_step < finalStep; ++adaptIter) {
      PetscLogDouble bytes;
      PetscInt       incSteps;
      ierr = PetscMemoryGetCurrentUsage(&bytes);CHKERRQ(ierr);
      ierr = PetscInfo4(ts, "AMR time step loop %D (%D, %g): memory used %g\n", adaptIter, ctx->current_step, ctx->current_time, bytes);CHKERRQ(ierr);
      if (targetCells > 0) SETERRQ(PETSC_COMM_WORLD,PETSC_ERR_SUP,"AMR with target cell count not implemented yet.");
      ierr = adaptToleranceFEM(ctx->fe,ts,uu,refineTol,coarsenTol,ctx,&tsNew,&solNew);CHKERRQ(ierr);
      if (tsNew) {
        ierr = PetscInfo(ts, "AMR used\n");CHKERRQ(ierr);
        ierr = DMDestroy(&ctx->dm);CHKERRQ(ierr);
        ierr = DMDestroy(&ctx->serial_dm);CHKERRQ(ierr);
        ierr = VecDestroy(&uu);CHKERRQ(ierr);
        ierr = TSDestroy(&ts);CHKERRQ(ierr);
        ts   = tsNew;
        uu   = solNew;
	ierr = TSSetTimeStep(ts,1.e-5);CHKERRQ(ierr);
        ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
        ierr = TSGetDM(ts, &ctx->dm);CHKERRQ(ierr);
	ierr = DMPlexGetRedundantDM(ctx->dm, &ctx->serial_dm);CHKERRQ(ierr);
	ierr = PetscObjectReference((PetscObject)ctx->dm);CHKERRQ(ierr);
        /* setup mesh data and solver -- ex11 did not do this? */
        ierr = MatDestroy(&ctx->M);CHKERRQ(ierr);
        ierr = MatDestroy(&ctx->J);CHKERRQ(ierr);
        ierr = MatDestroy(&J);CHKERRQ(ierr);
        ierr = DMCreateMatrix(ctx->dm, &ctx->J);CHKERRQ(ierr);
        ierr = DMCreateMatrix(ctx->dm, &J);CHKERRQ(ierr);
        ierr = TSSetIFunction(ts,NULL,LandIFunction,ctx);CHKERRQ(ierr);
        ierr = TSSetIJacobian(ts,J,J,LandIJacobian,ctx);CHKERRQ(ierr);
        ierr = LandCreateMassMatrix(ctx,uu,&ctx->M);CHKERRQ(ierr);
	if (ctx->verbose > 2 && !ctx->sub_index) {
	  char buf[256];
	  ierr = PetscSNPrintf(buf, 256, "%s-amr", ctx->plot_file_prefix);CHKERRQ(ierr);
	  ierr = LandDMVecView(uu, adaptIter, buf);CHKERRQ(ierr);
	}
      } else {
        ierr = PetscInfo(ts, "AMR not used\n");CHKERRQ(ierr);
      }
      ierr = TSSetFromOptions(ts);CHKERRQ(ierr);
      ierr = TSSetTime(ts, ctx->current_time);CHKERRQ(ierr);
      ierr = TSSolve(ts, uu);CHKERRQ(ierr); /* advance more */
      ierr = TSGetTime(ts, &ctx->current_time);CHKERRQ(ierr);
      ierr = TSGetStepNumber(ts, &incSteps);CHKERRQ(ierr);
      ctx->current_step += incSteps;
    }
  }
  {
  }
  if (ctx->verbose > 0) {
    DM          plex;
    PetscInt    cStart, cEnd;
    PetscScalar unorm,minmaxnorm[2];
    ierr = DMConvert(ctx->serial_dm, DMPLEX, &plex);CHKERRQ(ierr);
    ierr = DMPlexGetHeightStratum(plex,0,&cStart,&cEnd);CHKERRQ(ierr);
    ierr = DMDestroy(&plex);CHKERRQ(ierr);
    Nelem = cEnd - cStart;
    ierr = VecNorm(uu,NORM_2,&unorm);CHKERRQ(ierr);
    ierr = MPI_Allreduce(&unorm,&minmaxnorm[0],1,MPIU_REAL,MPI_MIN,PETSC_COMM_WORLD);CHKERRQ(ierr);
    ierr = MPI_Allreduce(&unorm,&minmaxnorm[1],1,MPIU_REAL,MPI_MAX,PETSC_COMM_WORLD);CHKERRQ(ierr);
    PetscPrintf(PETSC_COMM_WORLD, "%D TS time steps, %D cells, %D equations, Nq=%D (%D IPs, %D species), T=%g, norm diff=%g, %s precision, %s BC, %D MPI/solve, VL=%D\n",
		ctx->current_step,Nelem,Neq,Nq,Nelem*Nq,ctx->num_species,ctx->current_time,minmaxnorm[1]-minmaxnorm[0],
#if defined(PETSC_USE_REAL_SINGLE)
		"Single",
#else
		"Double",
#endif
#if defined(LAND_ADD_BCS)
		"Dirichlet",
#else
		"Neumann",
#endif
		sub_mpi_size,LAND_VL);
  }
  ierr = MatDestroy(&ctx->M);CHKERRQ(ierr);
  ierr = MatDestroy(&J);CHKERRQ(ierr);
  ierr = VecDestroy(&uu);CHKERRQ(ierr);
  ierr = MatDestroy(&ctx->J);CHKERRQ(ierr);
  for (ii=0;ii<ctx->num_species;ii++) {
    ierr = PetscFEDestroy(&ctx->fe[ii]);CHKERRQ(ierr);
  }
  ierr = DMDestroy(&ctx->serial_dm);CHKERRQ(ierr);
  ierr = TSDestroy(&ts);CHKERRQ(ierr);
  ierr = DMDestroy(&ctx->dm);CHKERRQ(ierr);
  if (ctx->comm != PETSC_COMM_WORLD && ctx->comm != PETSC_COMM_SELF) MPI_Comm_free(&ctx->comm);
  ierr = PetscFinalize();
  PetscFunctionReturn(ierr);
}


