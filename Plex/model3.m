for sid=1:3
    sid
    SDE = [1.01,1.34,1.88]';
    T1 = [0.21,.28,.38]';
    T272 = [0.48,.80,1.38]';
    A = [1,1,1;1,2,4;1,3,9];
    fT1   = A(sid,:)'.*(A\T1);
    fT272 = A(sid,:)'.*(A\T272);
    fF    = A(sid,:)'.*(A\SDE);
    pT1   = fT1./T1(sid)*100
    pT272 = fT272./T272(sid)*100
    pF    = fF./SDE(sid)*100
    flops_sec = SDE*272./T272
    percent_peak = flops_sec/2611*100
end
