% ---------------------------------------------------------
\begin{frame}{Collisional evolution of a distribution function}	
\begin{itemize}
\item Under small-angle dominated Coulomb collision, the distribution $f(\bm{v})$ evolves according to 
\begin{equation}
\frac{\partial f}{\partial t}= \nu_c\frac{\partial}{\partial\bm{v}}\cdot\int_{\mathbb{R}^3} d\bm{\bar{u}}\;\mathbf{U}(\bm{v},\bm{\bar{v}})\cdot\left(\bar{f}\frac{\partial f}{\partial\bm{v}}-f\frac{\partial \bar{f}}{\partial\bm{\bar{v}}}\right).
\end{equation}
\item Here $\nu_c=e^4\ln\Lambda/(8\pi m^2\varepsilon_0^2)$ and $\bm{v}$ is the velocity. The quantities with an overbar are evaluated at $\bm{\bar{v}}$.
\item The Landau tensor $\mathbf{U}(\bm{v},\bm{\bar{v}})$ is a scaled projection matrix defined according to
\begin{equation}
\label{eq:landau_tensor}
\mathbf{U}(\bm{v},\bm{\bar{v}})=\frac{1}{\lvert\bm{v}-\bm{\bar{v}}\rvert^3}\left(\lvert\bm{v}-\bm{\bar{v}}\rvert^2\mathbf{I}-(\bm{v}-\bm{\bar{v}})(\bm{v}-\bm{\bar{v}})\right)
\end{equation}
and has an eigenvector $\bm{v}-\bm{\bar{v}}$ corresponding to a zero eigenvalue
\end{itemize}
\end{frame}

\begin{frame}{Switch to dimensionless coordinates}	
\begin{itemize}
\item Define new coordinates $\bm{x}=c^{-1}\bm{v}$ and $\bm{\bar{x}}=c^{-1}\bm{\bar{v}}$ so that
\begin{align}
\partial/\partial\bm{v}&=c^{-1}\nabla, &\partial/\partial\bm{\bar{v}}&=c^{-1}\bar{\nabla},\\
d\bm{v}&=c^3d\bm{x}, &d\bm{\bar{v}}&=c^3d\bm{\bar{x}},
\end{align}
\item The tensor $\mathbf{U}$ and time transform according to
\begin{equation}
\mathbf{U}(\bm{v},\bm{\bar{v}})=c^{-1}\mathbf{U}(\bm{x},\bm{\bar{x}}), \qquad t=\nu_c^{-1}\tau
\end{equation}
\item The dimensionless Landau operator thus becomes
\begin{equation}
\label{eq:landau}
\frac{\partial f}{\partial\tau}=\nabla\cdot\int_{\mathbb{R}^3} d\bm{\bar{x}}\;\mathbf{U}(\bm{x},\bm{\bar{x}})\cdot\left(\bar{f}\nabla f-f\bar{\nabla}\bar{f}\right).
\end{equation}
\end{itemize}
\end{frame}


% ---------------------------------------------------
\begin{frame}{Weak formulation for the Landau operator}
\begin{itemize}
\item Choose a domain $f:\Omega\mapsto\mathbb{R}$ and require 
\begin{equation}
\int_{\partial\Omega}\bm{J}\cdot d\bm{S}=0, \quad \textrm{with} \quad \bm{J}(\bm{x})= \int_{\Omega}d\bm{\bar{x}}\;\mathbf{U}(\bm{x},\bm{\bar{x}})\cdot\left(f\bar{\nabla}\bar{f}-\bar{f}\nabla f\right)
\end{equation} 
\item Multiply the Landau equation with function $\phi$ and integrate over $\Omega$
\begin{equation}
\int d\bm{x}\;\phi\;\frac{\partial f}{\partial \tau}=\int_{\Omega} d\bm{x}\;\nabla \phi\cdot \int_{\Omega}d\bm{\bar{x}}\;\mathbf{U}(\bm{x},\bm{\bar{x}})\cdot\left(f\bar{\nabla}\bar{f}-\bar{f}\nabla f\right).
\end{equation}
\item This is the starting point for constructing a conservative discretization.
\end{itemize}
\end{frame}

% ---------------------------------------------------
\begin{frame}{Continuous space conservation properties}
\begin{itemize}
\item Rearrange the integration order in the weak formulation to get
\begin{equation}
\int d\bm{x}\;\phi\;\frac{\partial f}{\partial \tau}=\frac{1}{2}\int_{\Omega} d\bm{x}\int_{\Omega}d\bm{\bar{x}}\left(\nabla \phi-\bar{\nabla}\bar{\phi}\right)\cdot\mathbf{U}\cdot\left(f\bar{\nabla}\bar{f}-\bar{f}\nabla f\right).
\end{equation}
\item The above expression trivially vanishes for the functions $\phi(\bm{x}) \in \{1,\bm{x}\}$, providing density and momentum conservation.
\item Also the energy ${\cal E}(\bm{x})=\lvert \bm{x}\rvert^2$ is conserved because $\mathbf{U}$ satisfies
\begin{equation}
\left(\nabla {\cal E} - \bar{\nabla}\bar{{\cal E}}\right)\cdot\mathbf{U}=0
\end{equation}
\item The functions $\phi(\bm{x}) \in \{1,\bm{x},{\cal E}(\bm{x})\}$ are thus referred to as {\it collisional invariants}.
\end{itemize}
\end{frame}
% ---------------------------------------------------------

\begin{frame}{Choose a finite dimensional Vector space}
\begin{itemize}
\item Assume a \textit{finite}-dimensional vector space $V_h$ spanned by the set $\{\lambda_{\ell}(\bm{x})\}_{\ell}$ and approximate $(f,\phi)\approx(f_h,\phi_h)$ according to
\begin{align}
f_h(\bm{x},\tau)&=\sum_{\ell}F_{\ell}(\tau)\lambda_{\ell}(\bm{x}), &\phi_h(\bm{x})&=\sum_{\ell}\phi_{\ell}\lambda_{\ell}(\bm{x}).
\end{align}
\item Denote $F=\{F_{\ell}\}_{\ell}$ for convenience and define the vector- and tensor-valued functionals
\begin{align}
\bm{K}[F](\tau,\bm{x})&=\sum_{\ell}F_{\ell}(\tau)\bm{K}_{\ell}(\bm{x}), \\
\mathbf{D}[F](\tau,\bm{x})&=\sum_{\ell}F_{\ell}(\tau)\mathbf{D}_{\ell}(\bm{x})
\end{align}
\item The vectors $\bm{K}_{\ell}$ and the tensors $\mathbf{D}_{\ell}$ are
\begin{align}
\label{eq:fric_diff_coef}
\bm{K}_{\ell}(\bm{x})&=\int_{\Omega}d\bm{\bar{x}}\;\mathbf{U}(\bm{x},\bm{\bar{x}})\cdot\bar{\nabla}\bar{\lambda}_{\ell}(\bm{\bar{x}}),\\ \mathbf{D}_{\ell}(\bm{x})&=\int_{\Omega}d\bm{\bar{x}}\;\mathbf{U}(\bm{x},\bm{\bar{x}})\;\bar{\lambda}_{\ell}(\bm{\bar{x}})
\end{align}
\end{itemize}
\end{frame}

% ----------------------------
\begin{frame}{Obtain an equation for the Degrees of Freedom}
\begin{itemize}
\item Substitute $f_h$ and $\phi_h$ into the continuous weak form and obtain the discrete (but time-continuous) weak formulation
\begin{equation}
\sum_{ij}\phi_i{\cal M}_{ij}\frac{\partial F_j}{\partial \tau}=\sum_{ij}\phi_i{\cal C}_{ij}[F]\;F_{j},
\end{equation}
\item The coefficient matrices are defined
\begin{align}
{\cal M}_{ij}&=\int_{\Omega} d\bm{x}\; \lambda_i\lambda_j,\\
{\cal C}_{ij}[F]&=\int_{\Omega} d\bm{x}\;\nabla\lambda_i\cdot\left(\bm{K}[F]\;\lambda_j-\mathbf{D}[F]\cdot\nabla\lambda_j\right),
\end{align}
\item Since the discrete weak form is to hold for arbitrary functions $\phi_h\in V_h$, we obtain the following nonlinear system of ordinary differential equations for the degrees-of-freedom $F$ 
\begin{equation}
\label{eq:dofs}
\sum_{j}{\cal M}_{ij}\frac{\partial F_j}{\partial \tau}=\sum_{j}{\cal C}_{ij}[F]\;F_{j},\qquad \forall i.
\end{equation}
\end{itemize}
\end{frame}

% ------------------------------
\begin{frame}{Prove the discrete conservation laws}
\begin{itemize}
\item Assume that $\psi \in V_h$. By definition, we then have
\begin{equation}
\int d\bm{x}\;\psi\;\frac{\partial f_h}{\partial \tau}=\sum_{ij}\psi_i{\cal M}_{ij}\frac{\partial F_j}{\partial t}=\sum_{ij}\psi_i{\cal C}_{ij}[F]\;F_{j}.
\end{equation}
\item Assuming all integrals to be evaluated using a quadrature rule with weights $w_q$ and points $\bm{\xi}_q$, we find
\begin{align}
&\sum_{ij}\psi_i{\cal C}_{ij}[F]F_{j}=\frac{1}{2}\sum_{ij\ell,pq}\;w_pw_qF_{\ell}F_j\psi_i\Big(\nabla\lambda_i(\bm{\xi}_q)-\nabla\lambda_i(\bm{\xi}_p)\Big)\nonumber\\
&\qquad\cdot\mathbf{U}(\bm{\xi}_q,\bm{\xi}_p)\cdot\Big(\nabla\lambda_{\ell}(\bm{\xi}_p)\lambda_j(\bm{\xi}_q)-\lambda_{\ell}(\bm{\xi}_p)\nabla\lambda_j(\bm{\xi}_q)\Big).
\end{align}
\item As long as $\{1,\bm{x},{\cal E}(\bm{x})\}\in V_h$, the exact conservation laws follow from choosing $\psi\equiv\sum_i\psi_i\lambda_i(\bm{x})=\{1,\bm{x},{\cal E}(\bm{x})\}$ and then observing
\begin{equation}
\sum_i\psi_i\Big(\nabla\lambda_i(\bm{\xi}_q)-\nabla\lambda_i(\bm{\xi}_p)\Big)\cdot\mathbf{U}(\bm{\xi}_q,\bm{\xi}_p)=0.
\end{equation}
\end{itemize}
\end{frame}
