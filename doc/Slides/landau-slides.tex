\documentclass[english,mathserif,10pt]{beamer}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{verbatim}
\usepackage[absolute,overlay]{textpos}
\usepackage{animate}
\usepackage{pdfpages}
\makeatletter
\newcommand\makebeamertitle{\frame{\maketitle}}
\AtBeginDocument{
  \let\origtableofcontents=\tableofcontents
  \def\tableofcontents{\@ifnextchar[{\origtableofcontents}{\gobbletableofcontents}}
  \def\gobbletableofcontents#1{\origtableofcontents}
}
\renewcommand\makebeamertitle{\frame[plain]{\maketitle}}
\usetheme{Frankfurt}
\setbeamertemplate{navigation symbols}{}
\usefonttheme[onlylarge]{structurebold}
\setbeamertemplate{footline}
{
  \begin{flushleft}
    \hspace{0.20cm}\vspace{-0.18cm}{\tiny \insertframenumber/\inserttotalframenumber}
  \end{flushleft}
}
\useinnertheme{circles}
\usecolortheme{beaver}
\usecolortheme{rose}
\usecolortheme{dolphin}
\setbeamercolor{frametitle}{fg=white,bg=blue}
\setbeamercovered{invisible}
\usepackage{multimedia}
\usepackage{hyperref}

\makeatother

\usepackage{babel}
\setbeamercolor{block title}{use=structure,fg=white,bg=black!75!black}
\setbeamercolor{block body}{use=structure,fg=black,bg=white!20!white}

\begin{document}

\title[plane]{Conservative Finite Element Method for the Landau Collision Integral for Fusion Plasma Physics}
\author{Eero Hirvijoki$^\star$, Mark F. Adams$^\dagger$}

\date{
{$^\star$Princeton Plasma Physics Laboratory\\ $^\dagger$Lawrence Berkeley National Laboratory} \\ . \\ {\bf "Conservative discretization of the Landau collision integral", arXiv 1611.07881}  \\}

\makebeamertitle

\begin{frame}{Introduction - Fusion}
\includegraphics[page=2,width=\textwidth]{KAUST-2016-12.pdf}
\end{frame}

\begin{frame}{The Fundamental Equations for Plasma Physics: Boltzmann+Maxwell}
\includegraphics[page=3,width=\textwidth]{KAUST-2016-12.pdf}
\end{frame}

\begin{frame}{Nonlinear multi-species collision}
\includegraphics[page=4,width=\textwidth]{KAUST-2016-12.pdf}
\end{frame}

\begin{frame}{Solver}
\includegraphics[page=5,width=\textwidth]{KAUST-2016-12.pdf}
\end{frame}

%\begin{frame}[plain]{Introduction - Fusion}
%\setbeamercolor{background canvas}{bg=}
%\includepdf[pages=2]{./KAUST-2016-12.pdf}
%And here I present \href{KAUST-2016-12.pdf}{a table}.
%\end{frame}

\input{./formulation-long}

\input{./conserve-test}

% ---------------------------------------------------------
\begin{frame}{Formulation Summary}
\begin{itemize}
\item As long as the finite dimensional vector space $V_h$ can present density, $1$, momentum, $\bm{x}$, and energy, ${\cal E}(\bm{x})$, exactly, finite-element method guarantees exact numerical conservation laws
\begin{equation*}
\textrm{Requirement:} \quad \{1,\bm{x},{\cal E}(\bm{x}) \} \in V_h
\end{equation*}
\item In the nonrelativistic problem, the space of piece-wise second order polynomials is sufficient: $V_h=P_2$
\item In the relativistic problem, it is not possible to present the definition for the relativistic energy ${\cal E}(\bm{x})=\sqrt{1+x^2}$ exactly with polynomials of any order.
\item The presented results generalize to systems with multiple species 
\end{itemize}
\end{frame}
% ----------------------------------------------

% ---------------------------------------------------------
\begin{frame}{2V}
\begin{itemize}
\item The strong magnetic guide field of fusion plasmas militates against 3V formulations.
\begin{itemize}
\item Stiff and expensive
\end{itemize}
\item Gyrokinetic formations integrate over $\theta$, gyro-motion, dimension, leaving $(r,z)$ coordinates. 
\item A magnetic field ($B_z$) and perpendicular velocity ($v_r$) defines a Larmor radius
\item Density is assume to be uniformly distributed around the Larmore radius 
\item Project this ring to $(r,z)$ plane with Elliptic integrals (E \& K)
\item Expensive: about 165 flops, including two $log()$, a $sqrt()$, and a $pow()$, to evaluate the Landau tensors $U_K$ and $U_D$
\begin{itemize}
\item Different $U$ for $K$ and $D$
\end{itemize}
\end{itemize}
\end{frame}
% ----------------------------------------------

% ---------------------------------------------------------
\begin{frame}{Solvers and High Performance Computing Implementation}
\begin{itemize}
\item Use the Portable Extensible Toolkit for Scientific computing (PETSc) library
\item Use PETSc's time integrators (Crank-Nicolson), nonlinear solvers (Newton), and linear solvers (LU).
\item We use the FEM and mesh management (Plex) infrastructure PETSc to deploy an HPC implementation of the Landau collision operator
\item First mocked up (Eero) in python, the implemented in C (M) with PETSc.
\end{itemize}
\end{frame}
% ----------------------------------------------

\input{./code-examples}
% ---------------------------------------------------------
\begin{frame}{Vectorization - a start}
\begin{itemize}
\item Flatten inner loop over cells and IP
\item Use OMP just to put threads on vector lanes of Intel MIC
\item  {\tt
PetscInt  nip = numCells*Nq;\\
\#pragma omp simd linear(ipidx) \\
for (ipidx = 0; ipidx < nip; ++ipidx)  \\
  \quad  PetscInt  qi = ipidx\%Nq; \\
  \quad const PetscInt  ei = ipidx/Nq; \\
  \quad ... \\	
\#pragma forceinline recursive \\
  \quad LandauTensor2D(xj,\&x[ipidx*dim],mask[ipidx],Ud,Uk); \\
  \quad /* K = U * grad(f) */ \\
  \quad for (j=0;j<dim;j++) gg2\_0 += Uk[dim*0+j]*dfi[j]*wi; \\
  \quad for (j=0;j<dim;j++) gg2\_1 += Uk[dim*1+j]*dfi[j]*wi; \\
  \quad /* D = -U * (I$\otimes f_x$) */ \\
 \quad  gg3\_00 -= Ud[0] * fxi * wi; \\
  \quad ... \\
}
\end{itemize}
\end{frame}
% ----------------------------------------------


% ---------------------------------------------------------
\begin{frame}[plain]{Closure}
\begin{itemize}
\item We have initial implementation of unstructured high order accurate, with exact discrete  momentum and energy conservation, Landau collision operator for highly magnetized plasmas.
\item Future work:
\begin{itemize}
\item Multiple species
\item Add electric field and relativistic Landau tensor to generate runaway electrons
\item Vectorize kernels for vector processing.
\item Define figure of merit for work vs. precision analysis.
\item Experimental analysis of work and time complexity vs. order of FE elements vs. accuracy, etc.
\end{itemize} 
\end{itemize} 
\end{frame}
% ----------------------------------------------
			
\end{document}
