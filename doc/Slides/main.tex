\documentclass[english,mathserif,10pt]{beamer}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{verbatim}
\usepackage[absolute,overlay]{textpos}

\makeatletter
\newcommand\makebeamertitle{\frame{\maketitle}}
\AtBeginDocument{
  \let\origtableofcontents=\tableofcontents
  \def\tableofcontents{\@ifnextchar[{\origtableofcontents}{\gobbletableofcontents}}
  \def\gobbletableofcontents#1{\origtableofcontents}
}
\renewcommand\makebeamertitle{\frame[plain]{\maketitle}}
\usetheme{Frankfurt}
\setbeamertemplate{navigation symbols}{}
\usefonttheme[onlylarge]{structurebold}
\setbeamertemplate{footline}
{
  \begin{flushleft}
    \hspace{0.20cm}\vspace{-0.18cm}{\tiny \insertframenumber/\inserttotalframenumber}
  \end{flushleft}
}
\useinnertheme{circles}
\usecolortheme{beaver}
\usecolortheme{rose}
\usecolortheme{dolphin}
\setbeamercolor{frametitle}{fg=white,bg=blue}
\setbeamercovered{invisible}
\usepackage{multimedia}
\usepackage{hyperref}

\makeatother

\usepackage{babel}
\setbeamercolor{block title}{use=structure,fg=white,bg=black!75!black}
\setbeamercolor{block body}{use=structure,fg=black,bg=white!20!white}

\begin{document}
\title{Conservative finite-element method for the Landau collision integral}
\author{{\bf Mark F. Adams} and Eero Hirvijoki}
\date{{\bf King Abdullah University of Science and Technology}\\ \today}
\makebeamertitle

% ---------------------------------------------------------
\begin{frame}{Introduction}	
\begin{itemize}
%\item For clarity, we consider only like-species collisions.
\item The collisional evolution of the distribution function $\bm{u}$ is of the form
\begin{equation}
\frac{\partial f}{\partial t}(\bm{u})= \frac{\Gamma}{2m^2}\nabla\cdot\int_{\mathbb{R}^3} d\bm{\bar{u}}\;\mathbf{U}(\bm{u},\bm{\bar{u}})\cdot\left(\bar{f}\nabla f-f\bar{\nabla} \bar{f}\right).
\end{equation}
where $\bm{\bar{u}}$, and the domain of the integral, are for another species for inter-species collisions.
\item Here $\Gamma=e^4\ln\Lambda/(4\pi\varepsilon_0^2)$, $\ln\Lambda$ is the Coulomb logarithm, $e$ and $m$ are the charge and mass, and $\bm{u}=\bm{p}/m$ is the momentum per rest mass. The quantities with an overbar are evaluated at $\bm{\bar{u}}$.
\item At non-relativistic limit, the tensor $\mathbf{U}(\bm{u},\bm{\bar{u}})$ is 
\begin{equation}
\label{eq:landau_tensor}
\mathbf{U}=\frac{1}{\lvert\bm{u}-\bm{\bar{u}}\rvert^3}\left(\lvert\bm{u}-\bm{\bar{u}}\rvert^2\mathbf{I}-(\bm{u}-\bm{\bar{u}})(\bm{u}-\bm{\bar{u}})\right).
\end{equation}
\item In the relativistic case, the tensor $\mathbf{U}(\bm{u},\bm{\bar{u}})$ is more complicated
\begin{equation}
\label{eq:beliaev_budker_tensor}
\mathbf{U}=\frac{r^2}{\bar{\gamma}\gamma w^3}\left(w^2\mathbf{I}-\bm{u}\bm{u}-\bm{\bar{u}}\bm{\bar{u}}+r(\bm{u}\bm{\bar{u}}+\bm{\bar{u}}\bm{u})\right),
\end{equation}
with $r=\gamma\bar{\gamma}-\bm{u}\cdot\bm{\bar{u}}/c^2$, $w=c\sqrt{r^2-1}$, $\gamma=\sqrt{1+u^2/c^2}$, $\bar{\gamma}=\sqrt{1+\bar{u}^2/c^2}$, and $c$ is the speed of light. %In the limit $c\rightarrow \infty$, the Beliaev-Budker tensor $\mathbf{U}$ reduces to Landau tensor and the relativistic normalized momenta reduce to the non-relativistic expressions for velocities.
\end{itemize}
\end{frame}
% ---------------------------------------------------

% ---------------------------------------------------
\begin{frame}{Weak formulation for the Landau operator}
\begin{itemize}
%\item Numerically, it is difficult to handle infinite domains.
\item Set the $\bm{u}$-domain according to $f:\Omega\mapsto\mathbb{R}$ and assume $f=0$ on the boundary (e.g., zero density at $c$). 
\item Use the boundary conditions, assume $(f,\phi)\in V:\Omega\mapsto\mathbb{R}$ with $V$ some \textit{infinite} vector space, and get the weak formulation
\begin{equation}
\int d\bm{u}\;\phi\;\frac{\partial f}{\partial t}=\frac{\Gamma}{2m^2}\int_{\Omega} d\bm{u}\;\nabla \phi\cdot \int_{\Omega}d\bm{\bar{u}}\;\mathbf{U}\cdot\left(f\bar{\nabla}\bar{f}-\bar{f}\nabla f\right).
\end{equation}
\item Rearrange the integration order on the right to get
\begin{equation}
\int d\bm{u}\;\phi\;\frac{\partial f}{\partial t}=\frac{\Gamma}{4m^2}\int_{\Omega} d\bm{u}\int_{\Omega}d\bm{\bar{u}}\left(\nabla \phi-\bar{\nabla}\bar{\phi}\right)\cdot\mathbf{U}\cdot\left(f\bar{\nabla}\bar{f}-\bar{f}\nabla f\right).
\end{equation}
\item For $\phi(\bm{u}) \in \{1,\bm{u}\}$, the manipulated expression vanishes identically. Further, $(\nabla{\cal E}(\bm{u})-\bar{\nabla}{\cal E}(\bm{\bar{u}}))\cdot\mathbf{U}(\bm{u},\bm{\bar{u}})$ vanishes for non-relativistic energy ${\cal E}(\bm{u})=u^2$ and relativistic energy ${\cal E}(\bm{u})=\sqrt{1+u^2/c^2}$.
\item Thus $\phi(\bm{u}) \in \{1,\bm{u},{\cal E}(\bm{u})\}$ are collisional invariants. Density, momentum, and energy conservation.
\end{itemize}
\end{frame}
% ---------------------------------------------------------

% ---------------------------------------------------------
\begin{frame}{Discretization for $t$ and $\bm{u}$}
\begin{itemize}
\item Choose a \textit{finite} vector space $V_h$ spanned by the set $\{\lambda_{\ell}(\bm{u})\}_{\ell}$ and approximate $(f,\phi)\approx(f_h,\phi_h)$ according to
\begin{equation}
f_h(\bm{u},t)=\sum_{\ell}F_{\ell}(t)\lambda_{\ell}(\bm{u}) \qquad \phi_h(\bm{u})=\sum_{\ell}\phi_{\ell}\lambda_{\ell}(\bm{u})
\end{equation}
\item Define  $\bm{K}_{\ell}$ and $\mathbf{D}_{\ell}$ as
\begin{equation}
\label{eq:fric_diff_coef}
\bm{K}_{\ell}(\bm{u})=\int_{\Omega}d\bm{\bar{u}}\;\mathbf{U}\cdot\bar{\nabla}\bar{\lambda}_{\ell}\qquad\mathbf{D}_{\ell}(\bm{u})=\int_{\Omega}d\bm{\bar{u}}\;\mathbf{U}\;\bar{\lambda}_{\ell}
\end{equation}
\item The matrices ${\cal M}_{ij}$ and ${\cal C}_{ij}[F^{(k)}]$ are given by
\begin{align}
{\cal M}_{ij}&=\int_{\Omega} d\bm{u}\; \lambda_i\lambda_j,\\
{\cal C}_{ij}[F^{(k)}]&=\frac{\Gamma \delta t}{2m^2}\int_{\Omega} d\bm{u}\;\nabla\lambda_i\cdot\left(\bm{K}^{(k)}\lambda_j-\mathbf{D}^{(k)}\cdot\nabla\lambda_j\right),
\end{align}
\item A suitable implicit time integrator and nonlinear solver is then used to evolve the  system
\begin{equation}
\frac{\partial f}{\partial t} - {\cal C}(f) = 0,
\end{equation}
\end{itemize}
\end{frame}
% ---------------------------------------------------------

% ---------------------------------------------------------
\begin{frame}{Numerical conservation laws (1)}
\begin{itemize}
\item Choose the space $V_h$ so that the functions $\phi(\bm{u})=\{1,\bm{u},{\cal E}(\bm{u})\}$ are included in $V_h$ exactly, i.e., $\phi(\bm{u})\equiv\sum_i\phi^i\lambda_i(\bm{u})$. 
\item Compute $\phi$-moment of the numerical distribution function $f_h$ and compare its deviation between adjacent time steps 
\begin{align}
\int_{\Omega} d\bm{u}\;\phi\;\left(f_h^{(k)}-f_h^{(k-1)}\right)&=\sum_{ij}\phi_i{\cal M}_{ij}\left(F_j^{(k)}-F_j^{(k-1)}\right)\nonumber\\
&=\sum_{ij}\phi_i{\cal C}_{ij}[F^{(k)}]F^{(k)}_{j}+{\cal O}(\epsilon^{(n)})
\end{align}
\item Next we show that the term $\sum_{ij}\phi_i{\cal C}_{ij}[F^{(k)}]F^{(k)}_{j}$ vanishes identically. 
\item The error in the conservation laws is then directly controlled by the error in nonlinear solver for $F^{(k)}$. Note that for an explicit Euler in the time differentiation, the vector $\epsilon^{(n)}$ would be identically zero.
\end{itemize} 
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Numerical conservation laws (2)}
\begin{itemize}
\item Approximate the integrals over the domain $\Omega$ with a quadrature rule that consists of the points $\{\bm{\xi}_p\}_p$ and weights $\{w_p\}_p$
\item For the coefficients $\bm{K}^{(k)}(\bm{\xi}_p)$ and $\mathbf{D}^{(k)}(\bm{\xi}_p)$ we have
\begin{align}
\bm{K}^{(k)}(\bm{\xi}_p)&=\sum_{\ell}\sum_q\;w_q\;\mathbf{U}(\bm{\xi}_p,\bm{\xi}_q)\cdot\nabla\lambda_{\ell}(\bm{\xi}_q)F^{(k)}_{\ell}\\
\mathbf{D}^{(k)}(\bm{\xi}_p)&=\sum_{\ell}\sum_q\;w_q\;\mathbf{U}(\bm{\xi}_p,\bm{\xi}_q)\lambda_{\ell}(\bm{\xi}_q)F^{(k)}_{\ell}
\end{align}
\item Similarly we have for the term $\sum_{j}{\cal C}_{ij}[F^{(k)}]F^{(k)}_{j}$
\begin{align}
\sum_j{\cal C}_{ij}[F^{(k)}]F^{(k)}_j&=\frac{\Gamma \delta t}{2m^2}\sum_{j,p}w_pF^{(k)}_j\nabla\lambda_i(\bm{\xi}_p)\cdot\Big(\bm{K}^{(k)}(\bm{\xi}_p)\lambda_j(\bm{\xi}_p)\nonumber\\&\qquad\qquad-\mathbf{D}^{(k)}(\bm{\xi}_p)\cdot\nabla\lambda_j(\bm{\xi}_p)\Big).
\end{align}
\end{itemize} 
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Numerical conservation laws (3)}
\begin{itemize}
\item Substituting $\bm{K}^{(k)}(\bm{\xi}_p)$ and $\mathbf{D}^{(k)}(\bm{\xi}_p)$, the term $\sum_{j}{\cal C}_{ij}[F^{(k)}]F^{(k)}_{j}$ becomes
\begin{align}
\sum_j{\cal C}_{ij}[F^{(k)}]F^{(k)}_{j}&=\frac{\Gamma \delta t}{2m^2}\sum_{j\ell,pq}\;w_pw_qF^{(k)}_{\ell}F^{(k)}_j\nabla\lambda_i(\bm{\xi}_p)\cdot\mathbf{U}(\bm{\xi}_p,\bm{\xi}_q)\nonumber\\
&\qquad\qquad \cdot\left(\nabla\lambda_{\ell}(\bm{\xi}_q)\lambda_j(\bm{\xi}_p)-\lambda_{\ell}(\bm{\xi}_q)\nabla\lambda_j(\bm{\xi}_p)\right),
\end{align}
\item Since the expression is antisymmetric with respect to changing $j\leftrightarrow\ell$ and $p\leftrightarrow q$, one obtains
\begin{align}
\sum_j{\cal C}_{ij}[F^{(k)}]F^{(k)}_{j}&=\frac{\Gamma \delta t}{4m^2}\sum_{j\ell,pq}\;w_pw_qF^{(k)}_{\ell}F^{(k)}_j\Big(\nabla\lambda_i(\bm{\xi}_p)-\nabla\lambda_i(\bm{\xi}_q)\Big)\nonumber\\
&\qquad \cdot\mathbf{U}(\bm{\xi}_p,\bm{\xi}_q)\cdot\Big(\nabla\lambda_{\ell}(\bm{\xi}_q)\lambda_j(\bm{\xi}_p)-\lambda_{\ell}(\bm{\xi}_q)\nabla\lambda_j(\bm{\xi}_p)\Big),
\end{align}
\end{itemize} 
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Numerical conservation laws (4)}
\begin{itemize}
\item If we then take the inner product with $\phi_i$, we find 
\begin{align}
\sum_{ij}\phi_i{\cal C}_{ij}[F^{(k)}]F^{(k)}_{j}&=\frac{\Gamma \delta t}{4m^2}\sum_{ij\ell,pq}\;w_pw_qF^{(k)}_{\ell}F^{(k)}_j\phi_i\Big(\nabla\lambda_i(\bm{\xi}_p)-\nabla\lambda_i(\bm{\xi}_q)\Big)\nonumber\\
&\qquad \cdot\mathbf{U}(\bm{\xi}_p,\bm{\xi}_q)\cdot\Big(\nabla\lambda_{\ell}(\bm{\xi}_q)\lambda_j(\bm{\xi}_p)-\lambda_{\ell}(\bm{\xi}_q)\nabla\lambda_j(\bm{\xi}_p)\Big),
\end{align}
\item Since we are assuming an exact representation for $\phi(\bm{u})=\sum_i\phi_i\lambda_i(\bm{u})$, the expression $\sum_{ij}\phi_i{\cal C}_{ij}[F^{(k)}]F^{(k)}_{j}$ vanishes identically for $\phi(\bm{u}) \in \{1,\bm{u},{\cal E}(\bm{u})\}$ because of the term
\begin{equation}
\sum_i\phi_i\Big(\nabla\lambda_i(\bm{\xi}_p)-\nabla\lambda_i(\bm{\xi}_q)\Big)\cdot\mathbf{U}(\bm{\xi}_p,\bm{\xi}_q)\equiv \Big(\nabla\phi(\bm{\xi}_p)-\nabla\phi(\bm{\xi}_q)\Big)\cdot\mathbf{U}(\bm{\xi}_p,\bm{\xi}_q) 
\end{equation}
\item {\bf We want energy conservation, second moment, thus need a complete quadratic FE function space}
\end{itemize} 
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Null space of the continuous collision operator}
\begin{itemize}
\item Consider again the continuous operator 
\begin{equation}
\int d\bm{u}\;\phi\;\frac{\partial f}{\partial t}=\frac{\Gamma}{2m^2}\int_{\Omega} d\bm{u}\;\nabla \phi\cdot\int_{\Omega}d\bm{\bar{u}}\;\mathbf{U}\cdot\left(f\bar{\nabla}\bar{f}-\bar{f}\nabla f\right).
\end{equation}
\item Since $\phi$ is arbitrary, $f$ belongs to the null space of the collision operator if and only if 
\begin{equation}
\mathbf{U}\cdot(f\bar{\nabla}\bar{f}-\bar{f}\nabla f)\equiv\mathbf{U}\cdot f\bar{f}(\bar{\nabla}\ln\bar{f}-\nabla\ln f)=\bm{0}
\end{equation}
\item From the discussion regarding the conservation properties of the collision operator one quickly figures out that the solution to this equation is 
\begin{equation}
\ln f\propto a\;{\cal E}(\bm{u})+\bm{b}\cdot\bm{u}+c
\end{equation}
\item Here $a$, $\bm{b}$, and $c$ are arbitrary constants, with the restriction that $\int_{\mathbb{R}^3}d\bm{u}\; f < \infty$. In the non-relativistic case, this leads to $f$ being a Maxwellian with a possibility of a flow velocity.
\end{itemize} 
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Null space of the discrete collision operator}
\begin{itemize}
\item The discrete analog for the continuous null space condition is the requirement $\sum_j{\cal C}_{ij}[F]F_{j}= 0\; \forall i$. 
\item This leads to either the condition
\begin{multline}
\sum_{j\ell,pq}\;w_pw_q\nabla\lambda_i(\bm{\xi}_p)\cdot\mathbf{U}(\bm{\xi}_p,\bm{\xi}_q)\\
\cdot\left(F_{\ell}\nabla\lambda_{\ell}(\bm{\xi}_q)F_j\lambda_j(\bm{\xi}_p)-F_{\ell}\lambda_{\ell}(\bm{\xi}_q)_j\nabla\lambda_j(\bm{\xi}_p)\right)=0\; \forall i,
\end{multline}
or equivalently, after reordering the summation, to the condition
\begin{multline}
\sum_{j\ell,pq}\;w_pw_q(\nabla\lambda_i(\bm{\xi}_p)-\nabla\lambda_i(\bm{\xi}_q))\cdot\mathbf{U}(\bm{\xi}_p,\bm{\xi}_q)\\
\cdot\left(F_{\ell}\nabla\lambda_{\ell}(\bm{\xi}_q)F_j\lambda_j(\bm{\xi}_p)-F_{\ell}\lambda_{\ell}(\bm{\xi}_q)F_j\nabla\lambda_j(\bm{\xi}_p)\right)=0\; \forall i,
\end{multline}
\end{itemize} 
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Null space of the discrete collision operator (2)}
\begin{itemize}
\item Now we encounter an expression analogous to the continuous case
\begin{align}
&\mathbf{U}(\bm{\xi}_p,\bm{\xi}_q)\cdot\sum_{j\ell}\left(F_{\ell}\nabla\lambda_{\ell}(\bm{\xi}_q)F_j\lambda_j(\bm{\xi}_p)-F_{\ell}\lambda_{\ell}(\bm{\xi}_q)F_j\nabla\lambda_j(\bm{\xi}_p)\right)
\nonumber\\
&=\mathbf{U}(\bm{\xi}_p,\bm{\xi}_q)\cdot f_h(\bm{\xi}_q)f_h(\bm{\xi}_p)\left(\nabla \ln f_h(\bm{\xi}_q)-\nabla \ln f_h(\bm{\xi}_p)\right)
\end{align}
which is valid as long as none of the integration points coincide with the domain boundary $\partial\Omega$ where $f_h$ is set to be zero.
\item If this expression was to vanish for arbitrary choice of the quadrature points, the solution vector $F_j$ would have to satisfy the condition
\begin{equation}
 \ln \left(\sum_j F_j\lambda_j(\bm{u})\right) \propto a\;{\cal E}(\bm{u})+\bm{b}\cdot\bm{u}+c
\end{equation}
\item Since logarithm of a polynomial is no longer a polynomial, the condition cannot be met. Instead, the existence of a null-space will depend on the choice of the mesh and integration points and should thus be investigated numerically solving the equation $\sum_j{\cal C}_{ij}[F]F_{j}= 0\; \forall i$
\end{itemize} 
\end{frame}
% ----------------------------------------------


% ---------------------------------------------------------
\begin{frame}{Formulation Summary}
\begin{itemize}
\item As long as the set of basis functions $\{\lambda_i(\bm{u})\}$ living in the discrete vector space $V_h$ can present the global functions $\phi(\bm{u})=\{1,\bm{u},{\cal E}(\bm{u})\}$ exactly and the iterated solution to the standard FEM problem is accurate enough, the error in the numerical conservation laws is directly bounded by error in the iteration of the standard FEM problem.
\item In the non-relativistic case, a polynomial basis of second order is enough because the density (a constant), momentum (a linear function), and energy (a quadratic function) belong to the space of second order polynomials.
\item In the relativistic case, density and momentum are conserved but it is not possible to present the definition for the relativistic energy ${\cal E}(\bm{u})=\sqrt{1+u^2}$ exactly with polynomials.
\end{itemize}
\end{frame}
% ----------------------------------------------

% ---------------------------------------------------------
\begin{frame}{2V}
\begin{itemize}
\item The strong magnetic guide field of fusion plasmas militates against 3V formulations.
\begin{itemize}
\item Stiff and expensive
\end{itemize}
\item Gyrokinetic formations integrate over $\theta$, gyro-motion, dimension, leaving $(r,z)$ coordinates. 
\item A magnetic field ($B_z$) and perpendicular velocity ($v_r$) defines a Larmor radius
\item Density is assume to be uniformly distributed around the Larmore radius 
\item Project this ring to $(r,z)$ plane with Elliptic integrals (E \& K)
\item Complicated: about 165 flops, including two $log()$, a $sqrt()$, and a $pow()$, to evaluate the Landau tensors $U_K$ and $U_D$
\begin{itemize}
\item Different $U$ for $K$ and $D$
\end{itemize}
\end{itemize}
\end{frame}
% ----------------------------------------------


% ---------------------------------------------------------
\begin{frame}{Solvers and High Performance Computing Implementation}
\begin{itemize}
\item Use PETSc's time integrators (Crank-Nicolson), nonlinear solvers (Newton), and linear solvers (LU).
\item We use the FEM and mesh management (Plex) infrastructure in the Portable Extensible Toolkit for Scientific computing (PETSc) to deploy an HPC implementation of the Landau collision operator
\item First mocked up (Eero) in python, the implemented in C with PETSc.
\end{itemize}
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{PETSc}
\begin{itemize}
\item  {\tt
 for (qi=0;qi<Nq;qi++) \{ \\
 ...\\
    \quad LandauTensor2D(....\\
    \quad for (i=0;i<dim;i++) \{\\
      \quad \quad for (j=0;j<dim;j++) \\
      \quad \quad \quad  gg2[i] += Uk[dim*i+j]*dfi[j]*wi;\\
    \quad for (i=0;i<dim*dim;i++) gg3[i] -= Ud[i] * fxi * wi;\\
}
\item  {\tt
	/* Jacobian transform */\\
	for (d = 0; d < dim; ++d) \{\\
	  \quad for (d2 = 0, g2[d] = 0; d2 < dim; ++d2) \\
	  \quad\quad g2[d] += invJj[d*dim+d2]*gg2[d2]*wj;\\
	  for (dp = 0; dp < dim; ++dp) \{\\
	    \quad for (d2 = 0, g3[d*dim + dp] = 0; d2 < dim; ++d2) \{\\
	      \quad\quad for (d3 = 0; d3 < dim; ++d3) \{\\
		\quad\quad\quad g3[d*dim + dp] += invJj[d*dim+d2]*gg3[d2*dim+d3]*invJj[dp*dim+d3]*wj;\\
}
\end{itemize}
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{PETSc (continued)}
\begin{itemize}
\item  {\tt
for (d = 0; d < dim; ++d) \{\\
  \quad for (d2 = 0; d2 < dim; ++d2) \{\\
   \quad \quad  for (fidx = 0; fidx < Nb; ++fidx ) \{\\
    \quad \quad \quad   for (gidx=0, ...; gidx<Nb; ++gidx, fOff++) \{\\
	\quad \quad \quad \quad elemMat[fOff] += Dqj[fidx*dim+d]*g3[d*dim+d2]*Dqj[gidx*dim+d2] + Dqj[fidx*dim+d]*g2[d]*Bqj[gidx]*dinv; \\
}
\item { \tt
  DMPlexMatSetClosure(plex, section, globalSection, JacP, ej+cStart, elemMat, ADD\_VALUES);
}
\end{itemize}
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Python}
\begin{itemize}
\item  {\tt
   \# compute contributions from this quadrature point to Kk and Dk\\
   rKk+=np.dot(np.dot(Uk,grads.transpose()),fs[[y-1 for y in self.triToNodes[k]]])*qw[ix]*qx[ix,0]*r\\
   rDk+=Ud*np.dot(shapes,fs[[y-1 for y in self.triToNodes[k]]])*qw[ix]*qx[ix,0]*r\\
}
\item  {\tt
 \# contribution to the local matrix\\
 for i in range(self.nshapes)\\
 \quad    for j in range(self.nshapes)\\
 \quad \quad    c\_local[i,j] += np.dot(grads[i], rK*shapes[j]-np.dot(rD,grads[j]))*qw[ix]\\
}
\item  {\tt 
  \# Map the local contribution to the global matrix
  for i in range(self.nshapes)\\
  \quad   for j in range(self.nshapes)
  \quad \quad c\_global[self.triToNodes[k][i]-1,self.triToNodes[k][j]-1]\\
  \quad \quad \quad \quad +=c\_local[i][j]   \\
}
\end{itemize}
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Time slices of an initially bi-Maxwellian distribution function relaxing towards an equilibrium state}
  \includegraphics[width=.53\linewidth]{../figures/relaxation/landau-relaxation0000.png}%
\end{frame}
% ----------------------------------------------


% ----------------------------------------------
\begin{frame}{Time slices of an initially bi-Maxwellian distribution function relaxing towards an equilibrium state}
  \includegraphics[width=.53\linewidth]{../figures/relaxation/landau-relaxation0004.png}%
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Time slices of an initially bi-Maxwellian distribution function relaxing towards an equilibrium state}
  \includegraphics[width=.53\linewidth]{../figures/relaxation/landau-relaxation0008.png}%
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Time slices of an initially bi-Maxwellian distribution function relaxing towards an equilibrium state}
  \includegraphics[width=.53\linewidth]{../figures/relaxation/landau-relaxation0012.png}%
\end{frame}
% ----------------------------------------------



% ----------------------------------------------
\begin{frame}{Time slices of an initially bi-Maxwellian distribution function relaxing towards an equilibrium state}
  \includegraphics[width=.53\linewidth]{../figures/relaxation/landau-relaxation0016.png}%
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Time slices of an initially bi-Maxwellian distribution function relaxing towards an equilibrium state}
  \includegraphics[width=.53\linewidth]{../figures/relaxation/landau-relaxation0020.png}%
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Momentum conservation, measured with the innerproduct $\sum_{ij}\phi^i{\cal M}_{ij}F_j$ and $\phi=z$}
\begin{tabular}{llll}
$\tau$ & $\epsilon_{\textrm{tol}}=$\textrm{9.0E-01}  & $\epsilon_{\textrm{tol}}=$\textrm{1.0E-06} & $\epsilon_{\textrm{tol}}=$\textrm{1.0E-14} \\
\hline
0 & 3.97797664845241E-02              & 3.97797664845241E-02              & 3.97797664845248E-02 \\
1 & 3.9779{\color{red}1615978734}E-02 & 3.97797664{\color{red}781777}E-02 & 3.97797664845248e-02 \\
2 & 3.977{\color{red}88912692161}E-02 & 3.97797664{\color{red}743008}E-02 & 3.9779766484524{\color{red}7}E-02 \\
4 & 3.977{\color{red}86187222186}E-02 & 3.97797664{\color{red}643968}E-02 & 3.9779766484524{\color{red}7}E-02 \\
10& 3.977{\color{red}81761433900}E-02 & 3.97797664{\color{red}423772}E-02 & 3.9779766484524{\color{red}7}E-02 \\
20& 3.977{\color{red}75989653156}E-02 & 3.9779766{\color{red}3011934}E-02 & 3.9779766484524{\color{red}7}E-02 \\
\end{tabular}
\end{frame}
% ----------------------------------------------

% ----------------------------------------------
\begin{frame}{Energy conservation, measured with the innerproduct $\sum_{ij}\phi^i{\cal M}_{ij}F_j$ and $\phi=r^2+z^2$}
\begin{tabular}{llll}
$\tau$ & $\epsilon_{\textrm{tol}}=$\textrm{9.0E-01}  & $\epsilon_{\textrm{tol}}=$\textrm{1.0E-06} & $\epsilon_{\textrm{tol}}=$\textrm{1.0E-14} \\
\hline
0 & 3.17986788742740E-02              & 3.17986788742740E-02              & 3.17986788742740E-02 \\
1 & 3.17{\color{red}606259814932}E-02 & 3.179867{\color{red}90469298}E-02 & 3.17986788742740E-02 \\
2 & 3.17{\color{red}718385841557}E-02 & 3.179867{\color{red}94847238}E-02 & 3.1798678874274{\color{red}1}E-02 \\
4 & 3.1{\color{red}8140462853466}E-02 & 3.17986{\color{red}808661484}E-02 & 3.1798678874274{\color{red}1}E-02 \\
10& 3.1{\color{red}8822521328414}E-02 & 3.17986{\color{red}851426220}E-02 & 3.1798678874274{\color{red}1}E-02 \\ 
20& 3.1{\color{red}9077804661782}E-02 & 3.17986{\color{red}927385047}E-02 & 3.1798678874274{\color{red}2}E-02 \\
\end{tabular}
\end{frame}
% ----------------------------------------------

% ---------------------------------------------------------
\begin{frame}{Closure}
\begin{itemize}
\item We have initial implementation of unstructured, high order accurate with exact discrete  momentum and energy conservation, Landau collision operator for highly magnetized plasmas.
\item Future work:
\begin{itemize}
\item Add electric field and relativistic Landau tensor to generate runaway electrons
\item Vectorize kernels for vector processing.
\item Define figure of merit for work vs. precision analysis.
\item Experimental analysis of work and time complexity vs. order of FE elements vs. accuracy
\end{itemize} 
\end{itemize} 
\end{frame}
% ----------------------------------------------
			
\end{document}
