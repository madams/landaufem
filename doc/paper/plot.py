###!/usr/bin/env python
#import sys, os, math, glob
import matplotlib.pyplot as plt
import numpy as np
#import matplotlib.ticker as ticker
#from numpy import array
plt.close()
sizes = []
with open("Total-solve-AMR-output.txt", "r") as infile, open("Output.txt", "w") as outfile:
    for line in infile:
        words = line.split()
        #        outfile.write(line.split("<_|_>")[0
        sz = len(words)
        if sz > 17 and words[17] == 'cells)':
            nc = int(words[16])
            sizes.append(nc)
plt.figure(1)
plt.ylabel('Number of cells')
plt.xlabel('Time step')
#plt.title('History of number of cells')
n = len(sizes)
print n
x = np.arange(0,n,1)
plt.plot(x,sizes,'.')
print x
#ax.legend(ncol=2,loc="lower left", bbox_to_anchor=[0,0],shadow=False, fancybox=True,title="Number of cells")
plt.axis([0, 900, 60, 100])
# plt.show()
plt.savefig('no_cell_history.png', dpi=1000)
#plt.xticks(x, x, rotation='horizontal')
#plt.show()
