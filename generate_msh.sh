# generate the mesh file from the geometry file cube.geo
GMSH=`which gmsh`
if ["$GMSH" == ""]; then
    echo gmsh not found. Try:
    echo sudo apt-get install gmsh 
else
    echo generating mesh from cube.geo
    gmsh -3 cube.geo
fi
