# generate the mesh file from the geometry files
# cube.geo
# rzplate.geo
GMSH=`which gmsh`
if ["$GMSH" == ""]; then
    echo gmsh not found. Try:
    echo sudo apt-get install gmsh 
else
    echo generating mesh from rzplate.geo
    gmsh -2 -order 2 rzplate.geo
fi
