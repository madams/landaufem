// // Set one
// lc1=1.0; // characteristic size at the half-circle
// lc2=0.5; // characteristic size at the center

// // Set two
// lc1=1.0; // characteristic size at the half-circle
// lc2=0.3; // characteristic size at the center

// Set three
lc1=1.0; // characteristic size at the half-circle
lc2=0.2; // characteristic size at the center

L=2.0; // Length parameter

// Define five points from which the plane is constructed
Point(1) = {0.0, -L, 0.0, lc2};
Point(2) = {L, -L, 0.0, lc1};
Point(3) = {L, L, 0.0 ,lc1};
Point(4) = {0.0, L, 0.0, lc2};

// construct lines from the points
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

// construct the perimeter for the surface
Line Loop(5) = {1,2,3,4};

// construct the surface mesh to be inside the perimeter
Plane Surface(6) = {5};

// define the physical boundaries that dolfin should recognize
Physical Surface(0) = {6}; // the computational domain
Physical Line(0) = {4};  // the domain that will not have boundary value
Physical Line(1) = {1,2,3};  // the Dirichlet boundary
